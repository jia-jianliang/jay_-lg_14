import os.path


# 项目跟路径
BASEDIR = os.path.dirname(os.path.abspath(__file__))

# 截图存放路径
IMAGEDIR = BASEDIR + os.sep + "image" + os.sep

# 首页url
INDEX_URL = "http://192.168.206.139/index.php"