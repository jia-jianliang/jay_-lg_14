from selenium.webdriver.common.by import By

from page.index import Index

class TestLogin:

    def setup(self):
        self.index = Index()

    def teardown(self):
        self.index.driver.quit()

    def test_login_without_verify_code(self):
        self.index.goto_login().login("18610211944", "123456", "")
        self.index.screenshot()
        text = self.index.text(By.CLASS_NAME, "layui-layer-content")
        assert "验证码不能为空!", text
