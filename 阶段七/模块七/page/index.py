from selenium.webdriver.common.by import By

from page.base import Base
from page.login import Login


class Index(Base):

    def goto_login(self):
        # 点击登陆按钮
        self.click(By.CSS_SELECTOR, ".red")
        return Login(self.driver)
