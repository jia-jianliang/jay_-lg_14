from selenium.webdriver.common.by import By

from page.base import Base


class Login(Base):

    def login(self, username, password, verify_code):
        # 输入用户名
        self.input(By.ID, "username", username)
        # 输入密码
        self.input(By.ID, "password", password)
        # 输入验证码
        self.input(By.ID, "verify_code", verify_code)
        # 点击登陆
        self.click(By.NAME, "sbtbutton")

