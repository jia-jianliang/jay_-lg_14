import time

from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait

from config import IMAGEDIR


class Base:

    def __init__(self, driver:WebDriver=None):
        if driver:
            self.driver = driver
        else:
            self.driver = webdriver.Chrome()
            self.driver.get("http://192.168.206.139/index.php")
            self.driver.maximize_window()
        # 隐式等待
        self.driver.implicitly_wait(30)

    # 封装获取元素
    def find(self, by, loc):
        # return self.driver.find_element(by, loc)
        return WebDriverWait(self.driver, timeout=40).until(lambda x: self.driver.find_element(by, loc))
    # 点击元素
    def click(self, by, loc):
        return self.find(by, loc).click()

    # 输入字符串
    def input(self, by, loc, value):
        return self.find(by, loc).send_keys(value)

    # 获取文本
    def text(self, by, loc):
        return self.find(by, loc).text

    # 截图并放到根目录下image文件夹下
    def screenshot(self, filename=IMAGEDIR):
        name = time.strftime("%Y-%m-%d-%H-%M-%S")
        self.driver.get_screenshot_as_file(filename + name + ".png")

    # 判断元素是否存在
    def exists(self, by, loc):
        return True if self.find(by, loc) else False


