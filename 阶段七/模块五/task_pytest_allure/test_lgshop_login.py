import time
import allure
from selenium import webdriver
import pytest

class TestLGShopLogin:

    @classmethod
    def setup_class(cls):
        cls.driver = webdriver.Chrome()

    @classmethod
    def teardown_class(cls):
        time.sleep(2)
        cls.driver.quit()

    @allure.step(title="输入用户名")
    def test_enter_username(self):

        self.driver.get("http://192.168.206.139/index.php/Home/user/login.html")
        self.driver.find_element_by_id("username").send_keys("13800138006")

    @allure.step(title="输入密码")
    def test_enter_password(self):
        self.driver.find_element_by_id("password").send_keys("123456")

    @allure.step(title="输入验证码")
    def test_enter_verify_code(self):
        self.driver.find_element_by_id("verify_code").send_keys("6666")

    @allure.step(title="点击登陆按钮")
    def test_click_login_button(self):
        self.driver.find_element_by_link_text("登    录").click()


if __name__ == "__main__":
    pytest.main()
