import time

from selenium import webdriver


# 实例化driver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
# 打开登陆页面
login_url = "http://192.168.206.132/index.php/Home/user/login.html"
driver.get(login_url)

username = "13800138006"
password = "123456"
verify_code = "6666"

# 定位并输入用户名
driver.find_element(by=By.ID, value="username").send_keys(username)
# 密码
driver.find_element(by=By.ID, value="password").send_keys(password)
# 验证码
driver.find_element(by=By.ID, value="verify_code").send_keys(verify_code)
# 登陆
driver.find_element(by=By.CSS_SELECTOR, value=".J-login-submit").click()
# 首页
time.sleep(1)
driver.find_element(by=By.LINK_TEXT, value="首页").click()
driver.find_element(by=By.ID, value="q").send_keys("手机")
driver.find_element(by=By.CLASS_NAME, value="ecsc-search-button").click()
time.sleep(2)
# 点击列表页第一个商品
driver.find_element(by=By.XPATH, value="/html/body/div[4]/div/div[2]/div[2]/ul/li[1]/div/div[1]").click()
driver.find_element(by=By.LINK_TEXT, value="立即购买").click()
driver.find_element(by=By.ID, value="submit_order").click()
time.sleep(4)
# 获取订单号
order_info = driver.find_element(by=By.CLASS_NAME, value="succ-p")
order_list_info = order_info.text.split(" ")
for order in order_list_info:
    if order.isdigit():
        print(f"提交后的订单号是：{order}")
        break
