import time

from selenium import webdriver
from selenium.webdriver.common.by import By


# 实例化driver
driver = webdriver.Chrome()

regist_url = "file:///D:/%E7%99%BE%E5%BA%A6%E7%BD%91%E7%9B%98%E4%B8%8B%E8%BD%BD/%E6%B5%8B%E8%AF%95%E5%BC%80%E5%8F%9114%E6%9C%9F%E8%B5%84%E6%96%99/%E7%AC%AC%E4%B8%83%E9%98%B6%E6%AE%B5/%E6%A8%A1%E5%9D%972/%E6%B3%A8%E5%86%8CA/%E6%B3%A8%E5%86%8CA.html?userA=1&passwordA=1&telA=1&emailA=1&selecta=bj&fruit=lia"
# 打开页面
driver.get(regist_url)
# 输入用户名
# 账号：admin,密码：123456，电话：18600000000，电子邮件：123@qq.com
username = "admin"
password = "123456"
phone = "18600000000"
email = "123@qq.com"
# 定位并输入用户名
driver.find_element(by=By.CSS_SELECTOR, value="#userA").send_keys(username)
# 定位并输入密码
driver.find_element(by=By.CSS_SELECTOR, value="#passwordA").send_keys(password)
# 定位并输入手机号
driver.find_element(by=By.CSS_SELECTOR, value="#telA").send_keys(phone)
# 定位并输入邮箱
driver.find_element(by=By.CSS_SELECTOR, value="#emailA").send_keys(email)

time.sleep(2)
# 点击注册按钮
driver.find_element(by=By.CSS_SELECTOR, value="button[title='加入会员A']").click()

time.sleep(3)
driver.quit()
