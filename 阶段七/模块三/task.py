import time

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
driver = webdriver.Chrome()
url = "file:///D:/%E7%99%BE%E5%BA%A6%E7%BD%91%E7%9B%98%E4%B8%8B%E8%BD%BD/%E6%B5%8B%E8%AF%95%E5%BC%80%E5%8F%9114%E6%9C%9F%E8%B5%84%E6%96%99/%E7%AC%AC%E4%B8%83%E9%98%B6%E6%AE%B5/%E6%A8%A1%E5%9D%973/%E6%B3%A8%E5%86%8C%E5%AE%9E%E4%BE%8B/%E6%B3%A8%E5%86%8C%E5%AE%9E%E4%BE%8B/%E6%B3%A8%E5%86%8CA.html"


def task1():
    driver.get(url)
    # 点击alert按钮
    driver.find_element_by_id("alerta").click()
    # 获取alert弹窗文本
    alert_text = driver.switch_to.alert.text
    print("alert提示文本是: {}".format(alert_text))
    time.sleep(0.5)
    driver.switch_to.alert.accept()

def task2():
    driver.get(url)
    # 定位用户名
    username = driver.find_element_by_id("userA")
    username.send_keys("admin")
    # 实例化鼠标对象
    action_chains = ActionChains(driver)
    # 双击用户名全选
    action_chains.double_click(username).perform()
    # 注册按钮
    regist = driver.find_element_by_xpath("//button[@type='submitA']")
    # 移动鼠标到注册按钮
    action_chains.move_to_element(regist).perform()

def task3():
    url = "file:///D:/%E7%99%BE%E5%BA%A6%E7%BD%91%E7%9B%98%E4%B8%8B%E8%BD%BD/%E6%B5%8B%E8%AF%95%E5%BC%80%E5%8F%9114%E6%9C%9F%E8%B5%84%E6%96%99/%E7%AC%AC%E4%B8%83%E9%98%B6%E6%AE%B5/%E6%A8%A1%E5%9D%973/%E6%B3%A8%E5%86%8C%E5%AE%9E%E4%BE%8B/%E6%B3%A8%E5%86%8C%E5%AE%9E%E4%BE%8B/%E6%B3%A8%E5%86%8C%E5%AE%9E%E4%BE%8B.html"
    driver.get(url)
    # 填写主页面
    _get_element_by_driver(driver, "user", "password", "tel", "email")
    # 切换frameA页面
    driver.switch_to.frame("idframe1")
    # 填写frameA
    _get_element_by_driver(driver, "userA", "passwordA", "telA", "emailA")
    # 切换首页（普通页面）
    driver.switch_to.default_content()
    # 切换frameB
    driver.switch_to.frame("myframe2")
    _get_element_by_driver(driver, "userB", "passwordB", "telB", "emailB")


def _get_element_by_driver(dr, username, password, tel, email):

    driver.find_element_by_id(username).send_keys("user")
    # 密码
    driver.find_element_by_id(password).send_keys("password")
    # 手机号
    driver.find_element_by_id(tel).send_keys("tel")
    # 邮箱
    driver.find_element_by_id(email).send_keys("email")



if __name__ == "__main__":
    task1()
    # task2()
    # task3()