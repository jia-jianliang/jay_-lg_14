import random
import threading
import unittest
from threadpool import ThreadPool, makeRequests
import lib.HTMLTestRunner_PY3
from testcase.test_lgshop_login import TestLgShopLogin

def add_case():
    return unittest.defaultTestLoader.discover("./testcase", "test*.py")


def run(suite):

    with open(f"./report/report{threading.get_ident()}.html", 'wb') as f: # 打开要生成的测试报告文件
        # 实例化runner
        runner = lib.HTMLTestRunner_PY3.HTMLTestRunner(f)
        # 使用runner运行测试套件，得到测试结果
        result = runner.run(suite)
        # 使用runner生成测试报告
        runner.generateReport(suite, result)


if __name__ == '__main__':
    # 开启两个线程运行
    for i in range(2):
        threading.Thread(target=run, args=add_case()).start()
    # 实例化对象
    # thread1 = threading.Thread(target=run, args=add_case())
    # thread2 = threading.Thread(target=run, args=add_case())
    #
    # # 开始线程
    # thread1.start()
    # thread2.start()

# suite = unittest.TestSuite()
# # 将测试用例添加到测试套件
# suite.addTest(unittest.makeSuite(TestLgShopLogin))
# # 使用HTMLTestRunner运行测试用例，生成测试报告
# with open("./report/report.html", 'wb') as f: # 打开要生成的测试报告文件
#     # 实例化runner
#     runner = lib.HTMLTestRunner_PY3.HTMLTestRunner(f)
#     # 使用runner运行测试套件，得到测试结果
#     result = runner.run(suite)
#     # 使用runner生成测试报告
#     runner.generateReport(suite, result)
