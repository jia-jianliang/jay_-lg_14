import logging
import os.path
import unittest

import requests

from api.lgshop_login import LgShopLogin
from util import json_util
from parameterized import parameterized



class TestLgShopLogin(unittest.TestCase):

    data_file = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + "/data/login_data.json"

    def setUp(self) -> None:
        self.login = LgShopLogin()
        self.session = requests.Session()

    def tearDown(self) -> None:
        self.session.close()

    @parameterized.expand(json_util.read_data(data_file))
    def test001_login_success(self, test_case, username, password, message, status):
        logging.info(test_case)
        response = self.login.login_session(self.session, username, password)
        print(response.text)
        self.assertEqual(message, response.json().get("message"))