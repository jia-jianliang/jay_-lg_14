import json


def read_data(filename):
    with open(filename, mode='r', encoding='utf-8') as f:
        json_data = json.load(f)
        list_tuple_data = []
        for data in json_data:
            list_tuple_data.append(tuple(data.values()))
        return list_tuple_data


if __name__ == '__main__':
    read_data("../data/login_data.json")

