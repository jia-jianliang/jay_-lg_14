import logging

import requests


class LgShopLogin:

    def __init__(self):
        self.login_url = "http://192.168.206.138:8080/ssm_web/user/login?phone={username}&password={password}"

    def login(self):
        return requests.post(self.login_url)

    def login_session(self, session, username, password):
        return session.post(self.login_url.format(username=username, password=password))
