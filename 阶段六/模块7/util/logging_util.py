# 实例化日志器
import logging

import config
from logging import handlers


def init_logging():
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    # 创建控制器
    stream_header = logging.StreamHandler()
    print(config.BASE_PATH + "/log/lagou_log.log")
    file_handler = logging.handlers.TimedRotatingFileHandler(config.BASE_PATH + "/log/lagou_log.log", when='h', interval=1, backupCount=3, encoding="utf-8")
    # 日志格式字符串
    # fmt = "%(asctime)s [%(levelname)s] [%(funcName)s %(lineno)d] %(message)s"
    fmt = """(levelno)s: %(levelname)s: **%(filename)s:%(funcName)s: %(lineno)d: %(asctime)s: %(thread)d: %(threadName)s: %(message)s: 
    """
    formatter = logging.Formatter(fmt)
    # 将格式添加到处理器
    stream_header.setFormatter(formatter)
    file_handler.setFormatter(formatter)
    # 将处理器添加到日志器
    logger.addHandler(stream_header)
    logger.addHandler(file_handler)
    # 返回日志器
    return logger