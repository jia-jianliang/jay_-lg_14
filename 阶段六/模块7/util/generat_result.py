import sys

import config
from lib.HTMLTestRunner_PY3 import HTMLTestRunner


class GenarateResult:

    def __init__(self):
        self.result_list = []
        report_path = config.BASE_PATH + "/report/report.html"
        f = open(report_path, "wb")
        self.runner = HTMLTestRunner(f)

    def test_case(self, test_case):
        result = self.runner.run(test_case)
        self.result_list.append(result)
        return result

    def combine_result(self):
        result = self.result_list[0]
        for i in range(1, len(self.result_list)):
            for temp in self.result_list[i].result:
                if temp[0] == 0:
                    result.addSuccess(temp[1])
                if temp[0] == 1:
                    result.addFailure(temp[1], sys.exc_info())
                if temp[0] == 2:
                    result.addError(temp[1], sys.exc_info())
        return result