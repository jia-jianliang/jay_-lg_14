import logging

from config import *

class MysqlUtil:

    def backup_data(self, dataname=MYSQL_DATABASE, tablename=MYSQL_TABLENAME):
        back_file_name = BASE_PATH + "/backup/backup.sql"
        mysql_commond = "mysqldump --column-statistics=0"
        # mysql_commond = "C:\\Program Files\\MySQL\\MySQL Server 8.0\\bin\\mysqldump --column-statistics=0"
        sql = f"{mysql_commond} -u{MYSQL_USERNAME} -p{MYSQL_PASSWORD} -h{MYSQL_URL} -P{MYSQL_PORT} {dataname} {tablename} > {back_file_name}"
        os.system(sql)
        logging.info("数据备份成功！")

    def recovery(self,  dataname=MYSQL_DATABASE, sql_file_path=BASE_PATH + "/backup/backup.sql"):
        mysql_commond = "mysql"
        sql = f"{mysql_commond} -u{MYSQL_USERNAME} -p{MYSQL_PASSWORD} -h{MYSQL_URL} -P{MYSQL_PORT} {dataname} < {sql_file_path}"
        os.system(sql)
        logging.info("数据还原成功！")

    def init_data(self, filename, dataname=MYSQL_DATABASE):
        mysql_command = "mysql"
        sql = f"{mysql_command} -u{MYSQL_USERNAME} -p{MYSQL_PASSWORD} -h{MYSQL_URL} -P{MYSQL_PORT} {dataname} < {filename}"
        os.system(sql)
        logging.info("数据初始化成功")
