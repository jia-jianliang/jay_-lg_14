import hashlib
import config


def md5(data):
    data = config.AUTH_CODE + data
    return hashlib.md5(data.encode(encoding="utf-8")).hexdigest()
