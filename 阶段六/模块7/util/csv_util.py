import csv


def read_data(file):
    result_data = []
    with open(file, "r", encoding="utf-8") as f:
        data = csv.reader(f)
        count = 0
        for d in data:
            if count == 0:
                count += 1
                continue
            result_data.append(tuple(d))
    return result_data
