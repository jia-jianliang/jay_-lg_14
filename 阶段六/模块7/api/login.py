import config
import requests


class Login:

    def __init__(self):
        self.verify_url = config.BASE_URL + "/index.php?m=Home&c=User&a=verify&r=0.21003811629286262"
        self.login_url = config.BASE_URL + "/index.php?m=Home&c=User&a=do_login&t=0.22668384955842158"
        self.session = requests.session()

    def login(self, data):
        self.session.get(url=self.verify_url)
        return self.session.post(url=self.login_url, data=data)


if __name__ == '__main__':
    data = {
        "username": 13800138006,
        "password": 123456,
        "verify_code": 6666
    }
    print(Login().login(data))