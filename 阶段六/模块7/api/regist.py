import requests

import config


class Regist:

    def __init__(self):
        # 经过追溯源码发现注册的逻辑在\lagou2019\lagou_shop\application\home\controller\User.php 我的操作就是将User.php文件中的213
        # 行的判断取反并将220行的if判断取反去掉 目的就是绕过短信验证码的校验 达到成功注册的目的
        self.regist_url = config.BASE_URL + "/index.php/Home/User/reg.html"

    def regist(self, data):
        return requests.post(url=self.regist_url, data=data)


if __name__ == '__main__':
    data = {
        "auth_code": "TPSHOP",
        "scene": 1,
        "username": 18688888886,
        "verify_code": 6666,
        "password": "c96bc2f6606925da75da2b825f57e97b",
        "password2": "c96bc2f6606925da75da2b825f57e97b"
    }
    print(Regist().regist(data).json())