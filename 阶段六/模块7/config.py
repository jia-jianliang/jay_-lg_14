import os

AUTH_CODE = "TPSHOP"

# 服务器url
BASE_URL = "http://192.168.206.132"
# 本项目根目录
BASE_PATH = os.path.dirname(os.path.abspath(__file__))
BASE_HEADER = {
    "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
}


MYSQL_URL = "192.168.206.132"
MYSQL_PORT = 3306
MYSQL_USERNAME = "root"
MYSQL_PASSWORD = "123456"
MYSQL_DATABASE = "tpshop2.0"
MYSQL_CHARSET = "utf8"
MYSQL_TABLENAME = "tp_users"
