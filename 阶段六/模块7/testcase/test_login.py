import logging
import unittest

from parameterized import parameterized

from api.login import Login
from util import csv_util
import config


class TestLogin(unittest.TestCase):

    def setUp(self):
        self.login = Login()

    @parameterized.expand(csv_util.read_data(config.BASE_PATH + "/data/login.csv"))
    def test_login(self, username, password, verify_code, status, msg):
        data = {
            "username": username,
            "password": password,
            "verify_code": verify_code
        }

        response = self.login.login(data)
        logging.info("登陆")
        self.assertEqual(int(status), response.json().get("status"))
        self.assertEqual(msg, response.json().get("msg"))
