import logging
import unittest
from util.mysql_util import MysqlUtil
from parameterized import parameterized
import config
from util.md5_util import md5
from util.csv_util import read_data
from api.regist import Regist


class TestRegist(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.mysql_util = MysqlUtil()
        cls.mysql_util.backup_data()
        cls.mysql_util.init_data(config.BASE_PATH + "/backup/tp_users.sql")
        cls.regist = Regist()

    @classmethod
    def tearDownClass(cls) -> None:
        cls.mysql_util.recovery()

    @parameterized.expand(read_data(file=config.BASE_PATH + "/data/regist.csv"))
    #auth_code,scene,username,verify_code,code,password,password2
    def test_regist(self, auth_code, scene, username, verify_code, code, password, password2, status, msg):
        data = {
            "auth_code": auth_code,
            "scene": scene,
            "username": username,
            "verify_code": verify_code,
            "code": code,
            "password": md5(password),
            "password2": md5(password2)
        }
        response = self.regist.regist(data)
        logging.info("注册")
        self.assertEqual(int(status), response.json().get("status"))
        self.assertEqual(msg, response.json().get("msg"))