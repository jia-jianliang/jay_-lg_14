-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: 192.168.206.132    Database: tpshop2.0
-- ------------------------------------------------------
-- Server version	5.7.30-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tp_users`
--

DROP TABLE IF EXISTS `tp_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tp_users` (
  `user_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '琛╥d',
  `email` varchar(60) NOT NULL DEFAULT '' COMMENT '閭?欢',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '瀵嗙爜',
  `paypwd` varchar(32) DEFAULT NULL COMMENT '鏀?粯瀵嗙爜',
  `sex` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0 淇濆瘑 1 鐢?2 濂',
  `birthday` int(11) NOT NULL DEFAULT '0' COMMENT '鐢熸棩',
  `user_money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '鐢ㄦ埛閲戦?',
  `frozen_money` decimal(10,2) DEFAULT '0.00' COMMENT '鍐荤粨閲戦?',
  `distribut_money` decimal(10,2) DEFAULT '0.00' COMMENT '绱?Н鍒嗕剑閲戦?',
  `underling_number` int(5) DEFAULT '0' COMMENT '鐢ㄦ埛涓嬬嚎鎬绘暟',
  `pay_points` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '娑堣垂绉?垎',
  `address_id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '榛樿?鏀惰揣鍦板潃',
  `reg_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '娉ㄥ唽鏃堕棿',
  `last_login` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '鏈?悗鐧诲綍鏃堕棿',
  `last_ip` varchar(15) NOT NULL DEFAULT '' COMMENT '鏈?悗鐧诲綍ip',
  `qq` varchar(20) NOT NULL DEFAULT '' COMMENT 'QQ',
  `mobile` varchar(20) NOT NULL DEFAULT '' COMMENT '鎵嬫満鍙风爜',
  `mobile_validated` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '鏄?惁楠岃瘉鎵嬫満',
  `oauth` varchar(10) DEFAULT '' COMMENT '绗?笁鏂规潵婧?wx weibo alipay',
  `openid` varchar(100) DEFAULT NULL COMMENT '绗?笁鏂瑰敮涓?爣绀',
  `unionid` varchar(100) DEFAULT NULL,
  `head_pic` varchar(255) DEFAULT NULL COMMENT '澶村儚',
  `province` int(6) DEFAULT '0' COMMENT '鐪佷唤',
  `city` int(6) DEFAULT '0' COMMENT '甯傚尯',
  `district` int(6) DEFAULT '0' COMMENT '鍘',
  `email_validated` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '鏄?惁楠岃瘉鐢靛瓙閭??',
  `nickname` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '绗?笁鏂硅繑鍥炴樀绉',
  `level` tinyint(1) DEFAULT '1' COMMENT '浼氬憳绛夌骇',
  `discount` decimal(10,2) DEFAULT '1.00' COMMENT '浼氬憳鎶樻墸锛岄粯璁?涓嶄韩鍙',
  `total_amount` decimal(10,2) DEFAULT '0.00' COMMENT '娑堣垂绱??棰濆害',
  `is_lock` tinyint(1) DEFAULT '0' COMMENT '鏄?惁琚?攣瀹氬喕缁',
  `is_distribut` tinyint(1) DEFAULT '0' COMMENT '鏄?惁涓哄垎閿?晢 0 鍚?1 鏄',
  `first_leader` int(11) DEFAULT '0' COMMENT '绗?竴涓?笂绾',
  `second_leader` int(11) DEFAULT '0' COMMENT '绗?簩涓?笂绾',
  `third_leader` int(11) DEFAULT '0' COMMENT '绗?笁涓?笂绾',
  `token` varchar(64) DEFAULT '' COMMENT '鐢ㄤ簬app 鎺堟潈绫讳技浜巗ession_id',
  `message_mask` tinyint(1) NOT NULL DEFAULT '63' COMMENT '娑堟伅鎺╃爜',
  `push_id` varchar(30) NOT NULL DEFAULT '' COMMENT '鎺ㄩ?id',
  `distribut_level` tinyint(2) DEFAULT '0' COMMENT '鍒嗛攢鍟嗙瓑绾',
  `is_vip` tinyint(1) DEFAULT '0' COMMENT '鏄?惁涓篤IP 锛?涓嶆槸锛?鏄',
  `xcx_qrcode` varchar(255) DEFAULT NULL COMMENT '灏忕▼搴忎笓灞炰簩缁寸爜',
  `poster` varchar(255) DEFAULT NULL COMMENT '涓撳睘鎺ㄥ箍娴锋姤',
  PRIMARY KEY (`user_id`),
  KEY `email` (`email`),
  KEY `underling_number` (`underling_number`),
  KEY `mobile` (`mobile_validated`),
  KEY `openid` (`openid`),
  KEY `unionid` (`unionid`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tp_users`
--

LOCK TABLES `tp_users` WRITE;
/*!40000 ALTER TABLE `tp_users` DISABLE KEYS */;
INSERT INTO `tp_users` VALUES (1,'15915407513@163.com','519475228fe35ad067744465c42a19b2','519475228fe35ad067744465c42a19b2',0,-28800,100000.00,0.00,0.00,0,100000,0,1523235674,1523235674,'','','',0,'',NULL,NULL,'/public/upload/user/1/head_pic//1673d08c39ff9d1103611a7585a8ae0f.jpg',0,0,0,1,'15915407513@163.com',4,0.94,4939.90,0,0,0,0,0,'81953a80817fdf7c25e682ca3311abc9',63,'0',0,0,NULL,NULL),(2,'123','519475228fe35ad067744465c42a19b2','5317bc949fb15f19cdf1be43cf9a5ae6',2,662659200,1376.12,100.00,1120.00,1,57409,0,1523238708,1524877594,'','','15915407513',1,'',NULL,NULL,'/public/upload/head_pic/20180426/a803dc7558982208df046e7e9e558125.jpg',0,0,0,0,'15915407513',7,0.91,135988.54,0,1,0,0,0,'e9f5a1dc07f3674e5234667126f14d6a',63,'140fe1da9e8bbcb28d2',3,0,NULL,NULL),(3,'','519475228fe35ad067744465c42a19b2','90600d68b0f56d90c4c34284d8dfd138',0,1524067200,4057.47,0.00,0.00,0,49513,0,1523266058,0,'','','18515156363',1,'',NULL,NULL,'/public/upload/head_pic/20180419/fec83644525cdd715416db8789cfe222.jpg',0,0,0,0,'闈掓槬',4,0.94,9559.07,0,1,0,0,0,'',63,'190e35f7e07c8658ec6',0,0,NULL,NULL),(4,'15636363522@qq.com','519475228fe35ad067744465c42a19b2','519475228fe35ad067744465c42a19b2',0,-28800,0.00,0.00,0.00,0,110,0,1523266117,1523266117,'','','',0,'',NULL,NULL,'/public/upload/user/4/head_pic//7268f1d4ce9879694c2ec7da77d4dfbb.png',0,0,0,1,'15636363522@qq.com',1,1.00,0.00,0,0,0,0,0,'eaf8b23d80d7513ab4ffcf3b1129f668',63,'0',0,0,NULL,NULL),(5,'','519475228fe35ad067744465c42a19b2','519475228fe35ad067744465c42a19b2',0,0,49830.22,0.00,0.00,0,2410,0,1523588976,1523588976,'','','15766212618',1,'',NULL,NULL,'/public/images/icon_goods_thumb_empty_300.png',0,0,0,0,'15766212618',4,0.94,7687.08,0,1,0,0,0,'91fa24fa9b42dfc79fc37d0acec4ba51',63,'0',0,0,NULL,NULL),(6,'461799220@qq.com','519475228fe35ad067744465c42a19b2',NULL,0,0,1000.00,0.00,0.00,0,100,0,1523601798,0,'','461799220','18664316869',0,'',NULL,NULL,NULL,0,0,0,0,'鍗曟祴璇?',1,1.00,0.00,0,0,0,0,0,'',63,'',0,0,NULL,NULL),(7,'','519475228fe35ad067744465c42a19b2',NULL,0,0,0.00,0.00,0.00,0,100,0,1523608222,1523608222,'','','15274857485',1,'',NULL,NULL,'/public/images/icon_goods_thumb_empty_300.png',0,0,0,0,'15274857485',1,1.00,0.00,0,0,3,0,0,'001b3f89dc686ad2f53f5481e8c9fb30',63,'0',0,0,NULL,NULL),(8,'','519475228fe35ad067744465c42a19b2','519475228fe35ad067744465c42a19b2',0,0,96024.00,0.00,0.00,0,100000,0,1523857661,1646272277,'','','13800138006',1,'',NULL,NULL,'http://thirdwx.qlogo.cn/mmopen/vi_32/c58Iiaib1aPodvKHMMGR9ZYmq7XGFUgppvhxgQKrJxdlZTAauZ8dTucEguiamsncVDR3h32TMO4YzppDmSuHIGI9w/132',0,0,0,0,'summer',4,0.94,3976.00,0,0,3,0,0,'',63,'190e35f7e07c8658ec6',0,0,NULL,NULL),(9,'','519475228fe35ad067744465c42a19b2','519475228fe35ad067744465c42a19b2',2,1524153600,18527.80,0.00,0.00,0,100210,0,1523861478,0,'','1546515984','15274851525',0,'',NULL,NULL,'/public/upload/head_pic/20180420/b147d911d2d5b3ff252fa948d96fb5d3.jpg',0,0,0,0,'nana',2,1.00,871.20,0,1,0,0,0,'',63,'190e35f7e07c8658ec6',0,0,NULL,NULL),(10,'1522585@qq.com','519475228fe35ad067744465c42a19b2','519475228fe35ad067744465c42a19b2',2,-28800,5649.60,0.00,0.00,0,1100,0,1523864842,1524801341,'','','15919919433',0,'',NULL,NULL,'/public/upload/head_pic/20180423/a20e71149cc243fc2df4c6eb5caadbbd.jpg',0,0,0,0,'绛夊緟',4,0.94,6808.66,0,1,0,0,0,'cedbd4ca79a15888d4fd3a636834294f',63,'140fe1da9e8bbcb28d2',0,0,NULL,NULL),(11,'','519475228fe35ad067744465c42a19b2',NULL,0,0,0.00,0.00,0.00,0,100,0,1523933777,1523933777,'','','18585859674',1,'',NULL,NULL,'/public/images/icon_goods_thumb_empty_300.png',0,0,0,0,'18585859674',1,1.00,0.00,0,1,0,0,0,'2ba708bb3b18ff3b133d19802ddc8559',63,'0',0,0,NULL,NULL),(12,'1546515984@11.com','519475228fe35ad067744465c42a19b2','519475228fe35ad067744465c42a19b2',0,0,1824.00,0.00,0.00,0,2400,0,1524023190,1524023190,'','','18576762477',1,'',NULL,NULL,'/public/images/icon_goods_thumb_empty_300.png',0,0,0,0,'18576762477',2,1.00,88.00,0,1,0,0,0,'55a01f6a994fa693a5c6364839475bc9',63,'0',0,0,NULL,NULL),(13,'15274851694@qq.co','519475228fe35ad067744465c42a19b2',NULL,0,0,0.00,0.00,0.00,0,100,0,1524188790,0,'','','',0,'',NULL,NULL,NULL,0,0,0,0,'32',1,1.00,0.00,0,0,0,0,0,'',63,'',0,0,NULL,NULL),(14,'18516589423@qq.com','519475228fe35ad067744465c42a19b2',NULL,0,0,0.00,0.00,0.00,0,100,0,1524188838,0,'','','',0,'',NULL,NULL,NULL,0,0,0,0,'3434',1,1.00,0.00,0,0,0,0,0,'',63,'',0,0,NULL,NULL),(15,'','519475228fe35ad067744465c42a19b2',NULL,0,0,0.00,0.00,0.00,0,100,0,1524189831,1524189831,'','','15274851515',1,'',NULL,NULL,'/public/images/icon_goods_thumb_empty_300.png',0,0,0,0,'15274851515',2,1.00,0.00,0,1,0,0,0,'002322068d39e76fbc9afbaa98001828',63,'0',0,0,NULL,NULL),(16,'123@qq.com','519475228fe35ad067744465c42a19b2',NULL,1,0,0.00,0.00,0.00,0,100,0,1524470536,0,'','','13978520397',0,'',NULL,NULL,NULL,0,0,0,0,'125',3,1.00,0.00,0,0,0,0,0,'',63,'',0,0,NULL,NULL),(17,'','519475228fe35ad067744465c42a19b2','519475228fe35ad067744465c42a19b2',0,1524499200,8977.90,0.00,0.00,0,190,0,1524554349,1524822517,'','','15247471414',1,'',NULL,NULL,'/public/upload/head_pic/20180424/47684cc3e684a14cc2aae4a9294bf87e.jpg',0,0,0,0,'15247471414',3,0.99,1266.00,0,1,0,0,0,'af20aa77641d815ff645ee524d2cdd73',63,'190e35f7e07c8658ec6',0,0,NULL,NULL),(18,'','519475228fe35ad067744465c42a19b2',NULL,0,0,0.00,0.00,0.00,0,100,0,1524623385,1524623385,'','','15889560679',1,'',NULL,NULL,'/public/images/icon_goods_thumb_empty_300.png',0,0,0,0,'15889560679',2,1.00,0.00,0,1,0,0,0,'94c54e14ccdddf8f39ffa86262ea2b2e',63,'0',0,0,NULL,NULL),(19,'258282@163.com','519475228fe35ad067744465c42a19b2',NULL,0,0,0.00,0.00,0.00,0,100,0,1524706705,0,'','','15915407591',0,'',NULL,NULL,NULL,0,0,0,0,'娣诲姞浼氬憳',2,1.00,0.00,0,0,0,0,0,'',63,'',0,0,NULL,NULL),(20,'','519475228fe35ad067744465c42a19b2',NULL,0,0,0.00,0.00,0.00,0,0,0,1524714610,1524714610,'','','18515858596',1,'',NULL,NULL,'/public/images/icon_goods_thumb_empty_300.png',0,0,0,0,'18515858596',2,1.00,0.00,0,1,0,0,0,'2ffa0713419b11ca86b8f3c81744a387',63,'0',0,0,NULL,NULL),(21,'','519475228fe35ad067744465c42a19b2','519475228fe35ad067744465c42a19b2',0,1524672000,49256.00,0.00,0.00,0,100,0,1524723726,0,'','','15274851596',1,'',NULL,NULL,'/public/images/icon_goods_thumb_empty_300.png',0,0,0,0,'璺?汉鐢',2,1.00,744.00,0,1,3,0,0,'',63,'100d855909727631376',0,0,NULL,NULL),(22,'','519475228fe35ad067744465c42a19b2','519475228fe35ad067744465c42a19b2',0,0,57.62,0.00,0.00,0,44985,0,1524726905,1524738073,'','','13243434343',1,'',NULL,NULL,'/public/upload/head_pic/20180427/dd96cecf4bc0bd6414351cd9574d01e7.jpg',0,0,0,0,'13243434343',3,0.99,1760.14,0,1,0,0,0,'16bc4823350241a1d942777f58e74457',63,'',0,0,NULL,NULL),(23,'','519475228fe35ad067744465c42a19b2','519475228fe35ad067744465c42a19b2',0,0,996778.79,0.00,0.00,0,1997520,0,1524730504,1524824026,'','','18679683657',1,'',NULL,NULL,'https://thirdqq.qlogo.cn/qqapp/1106296395/B0C344E52B6012FDABEE9ECD0557C0CC/100',0,0,0,0,'18679683657',2,1.00,973.60,0,1,0,0,0,'4ce10de3f32357294373107e321a8b60',63,'190e35f7e07c8658ec6',0,0,NULL,NULL),(24,'','519475228fe35ad067744465c42a19b2','519475228fe35ad067744465c42a19b2',0,0,98942.66,0.00,210.00,1,700,0,1524795728,1524877186,'','','18516589423',1,'',NULL,NULL,'/public/upload/head_pic/20180427/76c696c4fef54d356f31ac6ac5e7836b.png',0,0,0,0,'18516589423',3,0.99,1503.34,0,1,0,0,0,'7eb905f8c6a08e6400e9d3d3e2c5e8f7',63,'100d855909727631376',3,0,NULL,NULL),(25,'424077952@qq.com','519475228fe35ad067744465c42a19b2',NULL,0,1524758400,900.00,100.00,0.00,0,1650,0,1524801616,0,'','','15915407197',0,'',NULL,NULL,'/public/upload/head_pic/20180427/349394fbb68b9b83d84acc8ea8b9bcae.jpg',0,0,0,0,'15915407197',4,0.94,8406.10,0,0,0,0,0,'',63,'140fe1da9e8bbcb28d2',0,0,NULL,NULL),(26,'','',NULL,0,0,0.00,0.00,0.00,0,0,0,1524807993,0,'','','',0,'wx','oGMnQ1HqUFRg4dum-GVONjO5jnno',NULL,'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIia8vDXHVeygaglict2Dl5T083zryibRSQznmOp5lpgjbyR8fwMcwgNhPAaY5LPHYGM0eOePV2icCJ2g/132',0,0,0,0,'鍜诲捇',2,1.00,152.00,0,1,0,0,0,'',63,'',0,0,NULL,NULL),(28,'','',NULL,1,0,0.00,0.00,0.00,0,0,0,1524823542,1524823548,'','','',0,'',NULL,NULL,'http://thirdwx.qlogo.cn/mmopen/PiajxSqBRaELISygfibpicKgw6UDPAMkC8heWGaoia4UWwzwKXfCOJ2yWPLMKVmPUAeeHdicOCGSniccDOiaRr7ribAxeQ/132',0,0,0,0,'鎵胯?',1,1.00,0.00,0,1,0,0,0,'d1ebce2f921d4d6a976523dc6c4c0bb8',63,'',0,0,NULL,NULL),(29,'','',NULL,1,0,0.00,0.00,0.00,0,0,0,1524823620,1524823620,'','','',0,'weixin','o9cTBwb9m33ZB0MCH3N816ZKAjfg',NULL,'http://thirdwx.qlogo.cn/mmopen/vi_32/h3xnlXLyIUwuUdl1tlHZGiatDgjthtMHUVkFg8tX9VPUGfnWHjDaynTKs0fMfWpoPLSOmCaVL5Es92iar7BPtnaA/132',0,0,0,0,'娴峰崡鏈?湡姝屾墜闄堝缓寮',1,1.00,0.00,0,1,0,0,0,'6f953ee36d49c10eada4b030feb57336',63,'',0,0,NULL,NULL),(30,'','',NULL,1,0,0.00,0.00,0.00,0,0,0,1524823875,1524823901,'','','',0,'',NULL,NULL,'http://thirdwx.qlogo.cn/mmopen/uJrMaWkMNhuNZVicusbq7252Wb3ia2mz9R5d4GYs7niatXvibiafDJQlcicTT9QEcFu8uMgia25yLRtlNyVOIo483KPLsuPaQEbIO4ic/132',0,0,0,0,'鐙?竴鏃犱簩',1,1.00,0.00,0,1,0,0,0,'3cafd0658e0a7184e63b252fc07023cf',63,'',0,0,NULL,NULL),(31,'','519475228fe35ad067744465c42a19b2','519475228fe35ad067744465c42a19b2',0,0,99498.00,0.00,0.00,0,100,0,1524825281,1524825281,'','','18516589633',1,'',NULL,NULL,'/public/images/icon_goods_thumb_empty_300.png',0,0,0,0,'18516589633',3,0.99,1019.00,0,1,24,0,0,'4f5b5aa5d7aed92baabd8a80616846e0',63,'0',0,0,NULL,NULL),(32,'','',NULL,0,0,0.00,0.00,0.00,0,0,0,1524825369,1524877618,'','','',0,'weixin','o9cTBwS0VxKjNkxe6NM9sYIorhAM',NULL,'/public/images/icon_goods_thumb_empty_300.png',0,0,0,0,'鍌呭倕',3,0.99,2378.60,0,1,2,0,0,'a552ca2c3b6ad9c02cf45afb44d7d6e4',63,'',0,0,NULL,NULL),(33,'','519475228fe35ad067744465c42a19b2','4710767206f1985084aee312c1e8c15c',2,0,0.00,0.00,0.00,0,0,0,1524825395,1524827692,'','','13923797247',0,'',NULL,NULL,NULL,0,0,0,0,'浣犲?寰楁嫢鏈',1,1.00,0.00,0,0,0,0,0,'7a790bd2706dc332db102b6b2080d2b1',63,'',0,0,NULL,NULL),(34,'','',NULL,1,0,0.00,0.00,0.00,0,0,0,1524828616,1524828729,'','','',0,'',NULL,NULL,'http://thirdwx.qlogo.cn/mmopen/1eiaFuQxQFhwfU2lUVgOuoLudu9NbLgOZB6GtoRAG5ZtOeOeMW9iaqegd2zF3ltgLmpQYmpF6xZGCPoibzMVT5PyVVuFQbLRpics/132',0,0,0,0,'鍝︻?',1,1.00,0.00,0,1,0,0,0,'5479315adb1092740b3533f5747355c0',63,'',0,0,NULL,NULL),(35,'','',NULL,1,0,0.00,0.00,0.00,0,0,0,1524834667,1524834676,'','','',0,'',NULL,NULL,'http://thirdwx.qlogo.cn/mmopen/1eiaFuQxQFhxicd52FQVGFnVc8HPcwxversDFAjm9MvIO0vibtiaGibrjnMyN7EnN9uEpJ8J4iaLeicw2z35wwmVZ8CPQnEMibh1YSF6/132',0,0,0,0,'鍛兼媺鑰',1,1.00,0.00,0,1,0,0,0,'70a24136b0a7608762722d8ccd8900a9',63,'',0,0,NULL,NULL),(36,'','',NULL,1,0,0.00,0.00,0.00,0,0,0,1524841123,1524841142,'','','',0,'',NULL,NULL,'http://thirdwx.qlogo.cn/mmopen/kYOuNqHYNuAHbhuCfOrlyDqibMK5Za5HRl7AVuq2ib0Je0UPMozwY9xQHxVcoIYUFvFhmgo1GeRViaxqXvAk8yhu61NBxLhmXF1/132',0,0,0,0,'鐫夸匠绉戞妧鏉庡仴',1,1.00,0.00,0,1,0,0,0,'78a50ff12ab85f8bc8221ecc2994306d',63,'',0,0,NULL,NULL),(37,'','',NULL,1,0,0.00,0.00,0.00,0,0,0,1524841193,1524841203,'','','',0,'',NULL,NULL,'http://thirdwx.qlogo.cn/mmopen/Q3auHgzwzM7Pct41m8E7oTHtGt6XncCmDgttK7s6Ftws0JibWNhQaC1vVpLrdbbqUKkwIAOxZIKtF30O235LKkw/132',0,0,0,0,'鐚?紦鍔',1,1.00,0.00,0,1,0,0,0,'d1c78c6eed15cc6f90cd00b4160c1a95',63,'',0,0,NULL,NULL),(38,'','',NULL,1,0,0.00,0.00,0.00,0,0,0,1524842949,1524842982,'','','',0,'',NULL,NULL,'http://thirdwx.qlogo.cn/mmopen/PiajxSqBRaEIXMLyayEJ2ehsKcQQ1qYmZxJUQs0bAbLk1KOd9QKnVU1dWrX6ZD2F6wDBWtn3xDLl0TlaDCeibSmw/132',0,0,0,0,'灏忚儭绉戞妧&瓒ｉ棯绉',1,1.00,0.00,0,1,0,0,0,'774231d44f815bf2380f196d63d18c15',63,'',0,0,NULL,NULL),(39,'','',NULL,1,0,0.00,0.00,0.00,0,0,0,1524877064,1524877079,'','','',0,'',NULL,NULL,'http://thirdwx.qlogo.cn/mmopen/PiajxSqBRaEJeHCYeLS7zGtp9yP3G7R6v2BH3sLCQvUgfNnlk7oqm1rHq11btzP96dnBF510PVbTpZDzF3yMfLQ/132',0,0,0,0,'[鐖卞績]绛夊緟鈥',1,1.00,0.00,0,1,0,0,0,'3d2660223342811f4f09170aed8d696b',63,'',0,0,NULL,NULL),(40,'','',NULL,1,0,0.00,0.00,0.00,0,0,0,1524877295,1524877295,'','','',0,'weixin','o9cTBwUDY08LVZUAXIvagXZrrp10',NULL,'http://thirdwx.qlogo.cn/mmopen/vi_32/cTRpAMg0XXiaeh8tSPPjib3dwobK7N79OzKL1ic42S9KbEKHuvTgWfRjPYphc7kXDsazdJAhlbDCseOPuenjC9KIg/132',0,0,0,0,'鐜',1,1.00,0.00,0,1,0,0,0,'54a80199155e63b5cc4a3dbc2a27dc84',63,'',0,0,NULL,NULL),(41,'','',NULL,2,0,0.00,0.00,0.00,0,0,0,1524877656,1524877664,'','','',0,'',NULL,NULL,'http://thirdwx.qlogo.cn/mmopen/uJrMaWkMNhuNZVicusbq729XNGSqZ965cqqf8R4icLP1yq9zGwOibtkohZz3Uz9N04Pib80x9MpPjLkQ8fZZ5Rly8Vgk8kVn3Hsg/132',0,0,0,0,'AM嗉娻緞嘟?銕曕繍',1,1.00,0.00,0,1,0,0,0,'9ca781b13e3dd896fd809ba46ddd9734',63,'',0,0,NULL,NULL),(42,'','',NULL,1,0,0.00,0.00,0.00,0,0,0,1524879067,1524879075,'','','',0,'',NULL,NULL,'http://thirdwx.qlogo.cn/mmopen/ajNVdqHZLLCccZkUHX5Lp2paNEKenmKOcC8NmSkrea4wOEPoel6FO8uRicIq3nddnLdLiaHBMeGuNWQbgpcxGRwg/132',0,0,0,0,'鍌查?',1,1.00,0.00,0,1,0,0,0,'240c4dece3a69e443dacfa5bde6cad4e',63,'',0,0,NULL,NULL),(51,'','519475228fe35ad067744465c42a19b2',NULL,0,0,0.00,0.00,0.00,0,0,0,1646293626,1646296865,'','','18610211944',1,'',NULL,NULL,'/public/images/icon_goods_thumb_empty_300.png',0,0,0,0,'18610211944',1,1.00,0.00,0,1,0,0,0,'93f525d4780cb60d4ff2349b1bebffdd',63,'0',0,0,NULL,NULL);
/*!40000 ALTER TABLE `tp_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-03 16:41:05
