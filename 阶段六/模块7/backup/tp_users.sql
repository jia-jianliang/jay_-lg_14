/*
Navicat MySQL Data Transfer

Source Server         : 192.168.206.132LGShop
Source Server Version : 50730
Source Host           : 192.168.206.132:3306
Source Database       : tpshop2.0

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2022-03-03 15:31:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tp_users
-- ----------------------------
DROP TABLE IF EXISTS `tp_users`;
CREATE TABLE `tp_users` (
  `user_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '表id',
  `email` varchar(60) NOT NULL DEFAULT '' COMMENT '邮件',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码',
  `paypwd` varchar(32) DEFAULT NULL COMMENT '支付密码',
  `sex` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0 保密 1 男 2 女',
  `birthday` int(11) NOT NULL DEFAULT '0' COMMENT '生日',
  `user_money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '用户金额',
  `frozen_money` decimal(10,2) DEFAULT '0.00' COMMENT '冻结金额',
  `distribut_money` decimal(10,2) DEFAULT '0.00' COMMENT '累积分佣金额',
  `underling_number` int(5) DEFAULT '0' COMMENT '用户下线总数',
  `pay_points` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '消费积分',
  `address_id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '默认收货地址',
  `reg_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '注册时间',
  `last_login` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `last_ip` varchar(15) NOT NULL DEFAULT '' COMMENT '最后登录ip',
  `qq` varchar(20) NOT NULL DEFAULT '' COMMENT 'QQ',
  `mobile` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号码',
  `mobile_validated` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否验证手机',
  `oauth` varchar(10) DEFAULT '' COMMENT '第三方来源 wx weibo alipay',
  `openid` varchar(100) DEFAULT NULL COMMENT '第三方唯一标示',
  `unionid` varchar(100) DEFAULT NULL,
  `head_pic` varchar(255) DEFAULT NULL COMMENT '头像',
  `province` int(6) DEFAULT '0' COMMENT '省份',
  `city` int(6) DEFAULT '0' COMMENT '市区',
  `district` int(6) DEFAULT '0' COMMENT '县',
  `email_validated` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否验证电子邮箱',
  `nickname` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '第三方返回昵称',
  `level` tinyint(1) DEFAULT '1' COMMENT '会员等级',
  `discount` decimal(10,2) DEFAULT '1.00' COMMENT '会员折扣，默认1不享受',
  `total_amount` decimal(10,2) DEFAULT '0.00' COMMENT '消费累计额度',
  `is_lock` tinyint(1) DEFAULT '0' COMMENT '是否被锁定冻结',
  `is_distribut` tinyint(1) DEFAULT '0' COMMENT '是否为分销商 0 否 1 是',
  `first_leader` int(11) DEFAULT '0' COMMENT '第一个上级',
  `second_leader` int(11) DEFAULT '0' COMMENT '第二个上级',
  `third_leader` int(11) DEFAULT '0' COMMENT '第三个上级',
  `token` varchar(64) DEFAULT '' COMMENT '用于app 授权类似于session_id',
  `message_mask` tinyint(1) NOT NULL DEFAULT '63' COMMENT '消息掩码',
  `push_id` varchar(30) NOT NULL DEFAULT '' COMMENT '推送id',
  `distribut_level` tinyint(2) DEFAULT '0' COMMENT '分销商等级',
  `is_vip` tinyint(1) DEFAULT '0' COMMENT '是否为VIP ：0不是，1是',
  `xcx_qrcode` varchar(255) DEFAULT NULL COMMENT '小程序专属二维码',
  `poster` varchar(255) DEFAULT NULL COMMENT '专属推广海报',
  PRIMARY KEY (`user_id`),
  KEY `email` (`email`),
  KEY `underling_number` (`underling_number`),
  KEY `mobile` (`mobile_validated`),
  KEY `openid` (`openid`),
  KEY `unionid` (`unionid`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_users
-- ----------------------------
INSERT INTO `tp_users` VALUES ('1', '15915407513@163.com', '519475228fe35ad067744465c42a19b2', '519475228fe35ad067744465c42a19b2', '0', '-28800', '100000.00', '0.00', '0.00', '0', '100000', '0', '1523235674', '1523235674', '', '', '', '0', '', null, null, '/public/upload/user/1/head_pic//1673d08c39ff9d1103611a7585a8ae0f.jpg', '0', '0', '0', '1', '15915407513@163.com', '4', '0.94', '4939.90', '0', '0', '0', '0', '0', '81953a80817fdf7c25e682ca3311abc9', '63', '0', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('2', '123', '519475228fe35ad067744465c42a19b2', '5317bc949fb15f19cdf1be43cf9a5ae6', '2', '662659200', '1376.12', '100.00', '1120.00', '1', '57409', '0', '1523238708', '1524877594', '', '', '15915407513', '1', '', null, null, '/public/upload/head_pic/20180426/a803dc7558982208df046e7e9e558125.jpg', '0', '0', '0', '0', '15915407513', '7', '0.91', '135988.54', '0', '1', '0', '0', '0', 'e9f5a1dc07f3674e5234667126f14d6a', '63', '140fe1da9e8bbcb28d2', '3', '0', null, null);
INSERT INTO `tp_users` VALUES ('3', '', '519475228fe35ad067744465c42a19b2', '90600d68b0f56d90c4c34284d8dfd138', '0', '1524067200', '4057.47', '0.00', '0.00', '0', '49513', '0', '1523266058', '0', '', '', '18515156363', '1', '', null, null, '/public/upload/head_pic/20180419/fec83644525cdd715416db8789cfe222.jpg', '0', '0', '0', '0', '青春', '4', '0.94', '9559.07', '0', '1', '0', '0', '0', '', '63', '190e35f7e07c8658ec6', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('4', '15636363522@qq.com', '519475228fe35ad067744465c42a19b2', '519475228fe35ad067744465c42a19b2', '0', '-28800', '0.00', '0.00', '0.00', '0', '110', '0', '1523266117', '1523266117', '', '', '', '0', '', null, null, '/public/upload/user/4/head_pic//7268f1d4ce9879694c2ec7da77d4dfbb.png', '0', '0', '0', '1', '15636363522@qq.com', '1', '1.00', '0.00', '0', '0', '0', '0', '0', 'eaf8b23d80d7513ab4ffcf3b1129f668', '63', '0', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('5', '', '519475228fe35ad067744465c42a19b2', '519475228fe35ad067744465c42a19b2', '0', '0', '49830.22', '0.00', '0.00', '0', '2410', '0', '1523588976', '1523588976', '', '', '15766212618', '1', '', null, null, '/public/images/icon_goods_thumb_empty_300.png', '0', '0', '0', '0', '15766212618', '4', '0.94', '7687.08', '0', '1', '0', '0', '0', '91fa24fa9b42dfc79fc37d0acec4ba51', '63', '0', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('6', '461799220@qq.com', '519475228fe35ad067744465c42a19b2', null, '0', '0', '1000.00', '0.00', '0.00', '0', '100', '0', '1523601798', '0', '', '461799220', '18664316869', '0', '', null, null, null, '0', '0', '0', '0', '单测试1', '1', '1.00', '0.00', '0', '0', '0', '0', '0', '', '63', '', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('7', '', '519475228fe35ad067744465c42a19b2', null, '0', '0', '0.00', '0.00', '0.00', '0', '100', '0', '1523608222', '1523608222', '', '', '15274857485', '1', '', null, null, '/public/images/icon_goods_thumb_empty_300.png', '0', '0', '0', '0', '15274857485', '1', '1.00', '0.00', '0', '0', '3', '0', '0', '001b3f89dc686ad2f53f5481e8c9fb30', '63', '0', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('8', '', '519475228fe35ad067744465c42a19b2', '519475228fe35ad067744465c42a19b2', '0', '0', '96024.00', '0.00', '0.00', '0', '100000', '0', '1523857661', '1646272277', '', '', '13800138006', '1', '', null, null, 'http://thirdwx.qlogo.cn/mmopen/vi_32/c58Iiaib1aPodvKHMMGR9ZYmq7XGFUgppvhxgQKrJxdlZTAauZ8dTucEguiamsncVDR3h32TMO4YzppDmSuHIGI9w/132', '0', '0', '0', '0', 'summer', '4', '0.94', '3976.00', '0', '0', '3', '0', '0', '', '63', '190e35f7e07c8658ec6', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('9', '', '519475228fe35ad067744465c42a19b2', '519475228fe35ad067744465c42a19b2', '2', '1524153600', '18527.80', '0.00', '0.00', '0', '100210', '0', '1523861478', '0', '', '1546515984', '15274851525', '0', '', null, null, '/public/upload/head_pic/20180420/b147d911d2d5b3ff252fa948d96fb5d3.jpg', '0', '0', '0', '0', 'nana', '2', '1.00', '871.20', '0', '1', '0', '0', '0', '', '63', '190e35f7e07c8658ec6', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('10', '1522585@qq.com', '519475228fe35ad067744465c42a19b2', '519475228fe35ad067744465c42a19b2', '2', '-28800', '5649.60', '0.00', '0.00', '0', '1100', '0', '1523864842', '1524801341', '', '', '15919919433', '0', '', null, null, '/public/upload/head_pic/20180423/a20e71149cc243fc2df4c6eb5caadbbd.jpg', '0', '0', '0', '0', '等待', '4', '0.94', '6808.66', '0', '1', '0', '0', '0', 'cedbd4ca79a15888d4fd3a636834294f', '63', '140fe1da9e8bbcb28d2', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('11', '', '519475228fe35ad067744465c42a19b2', null, '0', '0', '0.00', '0.00', '0.00', '0', '100', '0', '1523933777', '1523933777', '', '', '18585859674', '1', '', null, null, '/public/images/icon_goods_thumb_empty_300.png', '0', '0', '0', '0', '18585859674', '1', '1.00', '0.00', '0', '1', '0', '0', '0', '2ba708bb3b18ff3b133d19802ddc8559', '63', '0', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('12', '1546515984@11.com', '519475228fe35ad067744465c42a19b2', '519475228fe35ad067744465c42a19b2', '0', '0', '1824.00', '0.00', '0.00', '0', '2400', '0', '1524023190', '1524023190', '', '', '18576762477', '1', '', null, null, '/public/images/icon_goods_thumb_empty_300.png', '0', '0', '0', '0', '18576762477', '2', '1.00', '88.00', '0', '1', '0', '0', '0', '55a01f6a994fa693a5c6364839475bc9', '63', '0', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('13', '15274851694@qq.co', '519475228fe35ad067744465c42a19b2', null, '0', '0', '0.00', '0.00', '0.00', '0', '100', '0', '1524188790', '0', '', '', '', '0', '', null, null, null, '0', '0', '0', '0', '32', '1', '1.00', '0.00', '0', '0', '0', '0', '0', '', '63', '', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('14', '18516589423@qq.com', '519475228fe35ad067744465c42a19b2', null, '0', '0', '0.00', '0.00', '0.00', '0', '100', '0', '1524188838', '0', '', '', '', '0', '', null, null, null, '0', '0', '0', '0', '3434', '1', '1.00', '0.00', '0', '0', '0', '0', '0', '', '63', '', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('15', '', '519475228fe35ad067744465c42a19b2', null, '0', '0', '0.00', '0.00', '0.00', '0', '100', '0', '1524189831', '1524189831', '', '', '15274851515', '1', '', null, null, '/public/images/icon_goods_thumb_empty_300.png', '0', '0', '0', '0', '15274851515', '2', '1.00', '0.00', '0', '1', '0', '0', '0', '002322068d39e76fbc9afbaa98001828', '63', '0', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('16', '123@qq.com', '519475228fe35ad067744465c42a19b2', null, '1', '0', '0.00', '0.00', '0.00', '0', '100', '0', '1524470536', '0', '', '', '13978520397', '0', '', null, null, null, '0', '0', '0', '0', '125', '3', '1.00', '0.00', '0', '0', '0', '0', '0', '', '63', '', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('17', '', '519475228fe35ad067744465c42a19b2', '519475228fe35ad067744465c42a19b2', '0', '1524499200', '8977.90', '0.00', '0.00', '0', '190', '0', '1524554349', '1524822517', '', '', '15247471414', '1', '', null, null, '/public/upload/head_pic/20180424/47684cc3e684a14cc2aae4a9294bf87e.jpg', '0', '0', '0', '0', '15247471414', '3', '0.99', '1266.00', '0', '1', '0', '0', '0', 'af20aa77641d815ff645ee524d2cdd73', '63', '190e35f7e07c8658ec6', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('18', '', '519475228fe35ad067744465c42a19b2', null, '0', '0', '0.00', '0.00', '0.00', '0', '100', '0', '1524623385', '1524623385', '', '', '15889560679', '1', '', null, null, '/public/images/icon_goods_thumb_empty_300.png', '0', '0', '0', '0', '15889560679', '2', '1.00', '0.00', '0', '1', '0', '0', '0', '94c54e14ccdddf8f39ffa86262ea2b2e', '63', '0', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('19', '258282@163.com', '519475228fe35ad067744465c42a19b2', null, '0', '0', '0.00', '0.00', '0.00', '0', '100', '0', '1524706705', '0', '', '', '15915407591', '0', '', null, null, null, '0', '0', '0', '0', '添加会员', '2', '1.00', '0.00', '0', '0', '0', '0', '0', '', '63', '', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('20', '', '519475228fe35ad067744465c42a19b2', null, '0', '0', '0.00', '0.00', '0.00', '0', '0', '0', '1524714610', '1524714610', '', '', '18515858596', '1', '', null, null, '/public/images/icon_goods_thumb_empty_300.png', '0', '0', '0', '0', '18515858596', '2', '1.00', '0.00', '0', '1', '0', '0', '0', '2ffa0713419b11ca86b8f3c81744a387', '63', '0', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('21', '', '519475228fe35ad067744465c42a19b2', '519475228fe35ad067744465c42a19b2', '0', '1524672000', '49256.00', '0.00', '0.00', '0', '100', '0', '1524723726', '0', '', '', '15274851596', '1', '', null, null, '/public/images/icon_goods_thumb_empty_300.png', '0', '0', '0', '0', '路人甲', '2', '1.00', '744.00', '0', '1', '3', '0', '0', '', '63', '100d855909727631376', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('22', '', '519475228fe35ad067744465c42a19b2', '519475228fe35ad067744465c42a19b2', '0', '0', '57.62', '0.00', '0.00', '0', '44985', '0', '1524726905', '1524738073', '', '', '13243434343', '1', '', null, null, '/public/upload/head_pic/20180427/dd96cecf4bc0bd6414351cd9574d01e7.jpg', '0', '0', '0', '0', '13243434343', '3', '0.99', '1760.14', '0', '1', '0', '0', '0', '16bc4823350241a1d942777f58e74457', '63', '', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('23', '', '519475228fe35ad067744465c42a19b2', '519475228fe35ad067744465c42a19b2', '0', '0', '996778.79', '0.00', '0.00', '0', '1997520', '0', '1524730504', '1524824026', '', '', '18679683657', '1', '', null, null, 'https://thirdqq.qlogo.cn/qqapp/1106296395/B0C344E52B6012FDABEE9ECD0557C0CC/100', '0', '0', '0', '0', '18679683657', '2', '1.00', '973.60', '0', '1', '0', '0', '0', '4ce10de3f32357294373107e321a8b60', '63', '190e35f7e07c8658ec6', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('24', '', '519475228fe35ad067744465c42a19b2', '519475228fe35ad067744465c42a19b2', '0', '0', '98942.66', '0.00', '210.00', '1', '700', '0', '1524795728', '1524877186', '', '', '18516589423', '1', '', null, null, '/public/upload/head_pic/20180427/76c696c4fef54d356f31ac6ac5e7836b.png', '0', '0', '0', '0', '18516589423', '3', '0.99', '1503.34', '0', '1', '0', '0', '0', '7eb905f8c6a08e6400e9d3d3e2c5e8f7', '63', '100d855909727631376', '3', '0', null, null);
INSERT INTO `tp_users` VALUES ('25', '424077952@qq.com', '519475228fe35ad067744465c42a19b2', null, '0', '1524758400', '900.00', '100.00', '0.00', '0', '1650', '0', '1524801616', '0', '', '', '15915407197', '0', '', null, null, '/public/upload/head_pic/20180427/349394fbb68b9b83d84acc8ea8b9bcae.jpg', '0', '0', '0', '0', '15915407197', '4', '0.94', '8406.10', '0', '0', '0', '0', '0', '', '63', '140fe1da9e8bbcb28d2', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('26', '', '', null, '0', '0', '0.00', '0.00', '0.00', '0', '0', '0', '1524807993', '0', '', '', '', '0', 'wx', 'oGMnQ1HqUFRg4dum-GVONjO5jnno', null, 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIia8vDXHVeygaglict2Dl5T083zryibRSQznmOp5lpgjbyR8fwMcwgNhPAaY5LPHYGM0eOePV2icCJ2g/132', '0', '0', '0', '0', '咻咻', '2', '1.00', '152.00', '0', '1', '0', '0', '0', '', '63', '', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('28', '', '', null, '1', '0', '0.00', '0.00', '0.00', '0', '0', '0', '1524823542', '1524823548', '', '', '', '0', '', null, null, 'http://thirdwx.qlogo.cn/mmopen/PiajxSqBRaELISygfibpicKgw6UDPAMkC8heWGaoia4UWwzwKXfCOJ2yWPLMKVmPUAeeHdicOCGSniccDOiaRr7ribAxeQ/132', '0', '0', '0', '0', '承诺', '1', '1.00', '0.00', '0', '1', '0', '0', '0', 'd1ebce2f921d4d6a976523dc6c4c0bb8', '63', '', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('29', '', '', null, '1', '0', '0.00', '0.00', '0.00', '0', '0', '0', '1524823620', '1524823620', '', '', '', '0', 'weixin', 'o9cTBwb9m33ZB0MCH3N816ZKAjfg', null, 'http://thirdwx.qlogo.cn/mmopen/vi_32/h3xnlXLyIUwuUdl1tlHZGiatDgjthtMHUVkFg8tX9VPUGfnWHjDaynTKs0fMfWpoPLSOmCaVL5Es92iar7BPtnaA/132', '0', '0', '0', '0', '海南本土歌手陈建强', '1', '1.00', '0.00', '0', '1', '0', '0', '0', '6f953ee36d49c10eada4b030feb57336', '63', '', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('30', '', '', null, '1', '0', '0.00', '0.00', '0.00', '0', '0', '0', '1524823875', '1524823901', '', '', '', '0', '', null, null, 'http://thirdwx.qlogo.cn/mmopen/uJrMaWkMNhuNZVicusbq7252Wb3ia2mz9R5d4GYs7niatXvibiafDJQlcicTT9QEcFu8uMgia25yLRtlNyVOIo483KPLsuPaQEbIO4ic/132', '0', '0', '0', '0', '独一无二', '1', '1.00', '0.00', '0', '1', '0', '0', '0', '3cafd0658e0a7184e63b252fc07023cf', '63', '', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('31', '', '519475228fe35ad067744465c42a19b2', '519475228fe35ad067744465c42a19b2', '0', '0', '99498.00', '0.00', '0.00', '0', '100', '0', '1524825281', '1524825281', '', '', '18516589633', '1', '', null, null, '/public/images/icon_goods_thumb_empty_300.png', '0', '0', '0', '0', '18516589633', '3', '0.99', '1019.00', '0', '1', '24', '0', '0', '4f5b5aa5d7aed92baabd8a80616846e0', '63', '0', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('32', '', '', null, '0', '0', '0.00', '0.00', '0.00', '0', '0', '0', '1524825369', '1524877618', '', '', '', '0', 'weixin', 'o9cTBwS0VxKjNkxe6NM9sYIorhAM', null, '/public/images/icon_goods_thumb_empty_300.png', '0', '0', '0', '0', '傅傅', '3', '0.99', '2378.60', '0', '1', '2', '0', '0', 'a552ca2c3b6ad9c02cf45afb44d7d6e4', '63', '', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('33', '', '519475228fe35ad067744465c42a19b2', '4710767206f1985084aee312c1e8c15c', '2', '0', '0.00', '0.00', '0.00', '0', '0', '0', '1524825395', '1524827692', '', '', '13923797247', '0', '', null, null, null, '0', '0', '0', '0', '你值得拥有', '1', '1.00', '0.00', '0', '0', '0', '0', '0', '7a790bd2706dc332db102b6b2080d2b1', '63', '', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('34', '', '', null, '1', '0', '0.00', '0.00', '0.00', '0', '0', '0', '1524828616', '1524828729', '', '', '', '0', '', null, null, 'http://thirdwx.qlogo.cn/mmopen/1eiaFuQxQFhwfU2lUVgOuoLudu9NbLgOZB6GtoRAG5ZtOeOeMW9iaqegd2zF3ltgLmpQYmpF6xZGCPoibzMVT5PyVVuFQbLRpics/132', '0', '0', '0', '0', '哦', '1', '1.00', '0.00', '0', '1', '0', '0', '0', '5479315adb1092740b3533f5747355c0', '63', '', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('35', '', '', null, '1', '0', '0.00', '0.00', '0.00', '0', '0', '0', '1524834667', '1524834676', '', '', '', '0', '', null, null, 'http://thirdwx.qlogo.cn/mmopen/1eiaFuQxQFhxicd52FQVGFnVc8HPcwxversDFAjm9MvIO0vibtiaGibrjnMyN7EnN9uEpJ8J4iaLeicw2z35wwmVZ8CPQnEMibh1YSF6/132', '0', '0', '0', '0', '呼拉耶', '1', '1.00', '0.00', '0', '1', '0', '0', '0', '70a24136b0a7608762722d8ccd8900a9', '63', '', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('36', '', '', null, '1', '0', '0.00', '0.00', '0.00', '0', '0', '0', '1524841123', '1524841142', '', '', '', '0', '', null, null, 'http://thirdwx.qlogo.cn/mmopen/kYOuNqHYNuAHbhuCfOrlyDqibMK5Za5HRl7AVuq2ib0Je0UPMozwY9xQHxVcoIYUFvFhmgo1GeRViaxqXvAk8yhu61NBxLhmXF1/132', '0', '0', '0', '0', '睿佳科技李健', '1', '1.00', '0.00', '0', '1', '0', '0', '0', '78a50ff12ab85f8bc8221ecc2994306d', '63', '', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('37', '', '', null, '1', '0', '0.00', '0.00', '0.00', '0', '0', '0', '1524841193', '1524841203', '', '', '', '0', '', null, null, 'http://thirdwx.qlogo.cn/mmopen/Q3auHgzwzM7Pct41m8E7oTHtGt6XncCmDgttK7s6Ftws0JibWNhQaC1vVpLrdbbqUKkwIAOxZIKtF30O235LKkw/132', '0', '0', '0', '0', '猪鼓励', '1', '1.00', '0.00', '0', '1', '0', '0', '0', 'd1c78c6eed15cc6f90cd00b4160c1a95', '63', '', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('38', '', '', null, '1', '0', '0.00', '0.00', '0.00', '0', '0', '0', '1524842949', '1524842982', '', '', '', '0', '', null, null, 'http://thirdwx.qlogo.cn/mmopen/PiajxSqBRaEIXMLyayEJ2ehsKcQQ1qYmZxJUQs0bAbLk1KOd9QKnVU1dWrX6ZD2F6wDBWtn3xDLl0TlaDCeibSmw/132', '0', '0', '0', '0', '小胡科技&趣闪租', '1', '1.00', '0.00', '0', '1', '0', '0', '0', '774231d44f815bf2380f196d63d18c15', '63', '', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('39', '', '', null, '1', '0', '0.00', '0.00', '0.00', '0', '0', '0', '1524877064', '1524877079', '', '', '', '0', '', null, null, 'http://thirdwx.qlogo.cn/mmopen/PiajxSqBRaEJeHCYeLS7zGtp9yP3G7R6v2BH3sLCQvUgfNnlk7oqm1rHq11btzP96dnBF510PVbTpZDzF3yMfLQ/132', '0', '0', '0', '0', '[爱心]等待…', '1', '1.00', '0.00', '0', '1', '0', '0', '0', '3d2660223342811f4f09170aed8d696b', '63', '', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('40', '', '', null, '1', '0', '0.00', '0.00', '0.00', '0', '0', '0', '1524877295', '1524877295', '', '', '', '0', 'weixin', 'o9cTBwUDY08LVZUAXIvagXZrrp10', null, 'http://thirdwx.qlogo.cn/mmopen/vi_32/cTRpAMg0XXiaeh8tSPPjib3dwobK7N79OzKL1ic42S9KbEKHuvTgWfRjPYphc7kXDsazdJAhlbDCseOPuenjC9KIg/132', '0', '0', '0', '0', '王', '1', '1.00', '0.00', '0', '1', '0', '0', '0', '54a80199155e63b5cc4a3dbc2a27dc84', '63', '', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('41', '', '', null, '2', '0', '0.00', '0.00', '0.00', '0', '0', '0', '1524877656', '1524877664', '', '', '', '0', '', null, null, 'http://thirdwx.qlogo.cn/mmopen/uJrMaWkMNhuNZVicusbq729XNGSqZ965cqqf8R4icLP1yq9zGwOibtkohZz3Uz9N04Pib80x9MpPjLkQ8fZZ5Rly8Vgk8kVn3Hsg/132', '0', '0', '0', '0', 'AM༊྄ཻ ㎕࿐', '1', '1.00', '0.00', '0', '1', '0', '0', '0', '9ca781b13e3dd896fd809ba46ddd9734', '63', '', '0', '0', null, null);
INSERT INTO `tp_users` VALUES ('42', '', '', null, '1', '0', '0.00', '0.00', '0.00', '0', '0', '0', '1524879067', '1524879075', '', '', '', '0', '', null, null, 'http://thirdwx.qlogo.cn/mmopen/ajNVdqHZLLCccZkUHX5Lp2paNEKenmKOcC8NmSkrea4wOEPoel6FO8uRicIq3nddnLdLiaHBMeGuNWQbgpcxGRwg/132', '0', '0', '0', '0', '傲风', '1', '1.00', '0.00', '0', '1', '0', '0', '0', '240c4dece3a69e443dacfa5bde6cad4e', '63', '', '0', '0', null, null);
