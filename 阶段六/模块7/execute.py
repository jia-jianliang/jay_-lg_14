import threadpool
import unittest

import config
from util.generat_result import GenarateResult


gr = GenarateResult()
case = unittest.defaultTestLoader.discover(config.BASE_PATH + "/testcase", "test*.py")

pool = threadpool.ThreadPool(10)
req_list = threadpool.makeRequests(gr.test_case, case)
for req in req_list:
    pool.putRequest(req)
pool.wait()

result = gr.combine_result()
gr.runner.generateReport(result, result)