import java.util.Random;
import java.util.Scanner;

public class Test {

    /**
     * 1判断用户输入的年月日数据是一年中的第几天并打印
     * @return
     */
    public static int getManyDays(){
//
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入日期，格式请参照20220216");
        int date = sc.nextInt();
        int day = date % 100;
        int month = date /100 % 100;
        int year = date /10000;

        int sum = 0;
        //switch匹配月份，匹配到了加月份-1对应月份的天数，2月份区分闰月非闰月，所以区别对待  这里用到了switch的case穿透特性
        switch(month){
            case 12: sum+=30;
            case 11: sum+=31;
            case 10: sum+=30;
            case 9: sum+=31;
            case 8: sum+=31;
            case 7: sum+=30;
            case 6: sum+=31;
            case 5: sum+=30;
            case 4: sum+=31;
            case 3:;
            case 2: sum+=31;
            case 1: sum+=day;
        }
        //二月份单独算天数
        if(month > 2){
            //判断是否闰年，闰年二月29天， 非闰年二月28天
            if((year % 4 ==0 && year % 100 != 0) || year % 400 == 0){
                sum += 29;
            }else{
                sum += 28;
            }
        }
        System.out.println("您输入的日期：" + date + "是一年中的第" + sum + "天");
        return sum;
    }


    /**
     * 2 编程找出 1000 以内的所有完数并打印出来
     */
    public static void getPerfectNum(){
        for(int i = 1; i <= 1000; i++){
            int sum = 0;
            for(int j = 1; j < i; j++){
                if(i % j == 0){
                    sum += j;
                }
            }
            if(sum == i){
                System.out.println(i);
            }
        }
    }


    /**
     * 3 实现双色球抽奖游戏中奖号码的生成
     * @param
     */
    public static void getDoubleColorNum(){
        //声明一个存放双色球的int数组
        int[] arr = new int[7];
        Random random = new Random();
        //变例lenght-1次 用来存放生成的随机数
        for(int i = 0; i < arr.length-1; i++){
            int tem = random.nextInt(33) + 1;
            //遍历数组，保证生成的随机数没有重复
            for (int j = 0; j < arr.length-1; j++){
                while(arr[j] == tem) {
                    tem = random.nextInt(33) + 1;
                }
            }
            arr[i] = tem;

        }
        //生成红色球
        arr[6] = random.nextInt(16)+1;

        //遍历数组并打印
        for(int i = 0; i < arr.length; i++){
            if(i == arr.length-1){
                System.out.print("篮球是" + arr[i]);
                break;
            }
            System.out.println("双色球的红球是：" + arr[i]);
        }
    }

    /**
     * 4 编程题 自定义数组扩容规则
     * @param
     */
    public static void customArrayRule(){
        //根据用户输入创建对应容量数组
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入数组容量");
        int[] arr = new int[sc.nextInt()];
        int count = 0;
        while(true) {
            //根据用户输入进行数组元素赋值，当容量达到0.8后，新创建一个1.5倍容量数组并把扩容前数据复制到新数组中
            if (count * 1.0 /arr.length >= 0.8 || count == arr.length-1){
                int[] brr = new int[(int)(arr.length * 1.5)];
                for (int i = 0; i < count; i++){
                    brr[i] = arr[i];
                }
                arr = brr;
                System.out.println("数组已扩容扩容后的lenght是： " + arr.length);
            }
            System.out.println("请输入数组第" + (count + 1) + "个数，退出请输入-1");

            int num = sc.nextInt();
            if (num == -1){
                System.out.println("bye!");
                break;
            }
            arr[count] = num;
            count++;
        }
    }


    /**
     *  5 编程题 使用二维数组和循环实现五子棋游戏棋盘
     * @param
     */
    public static void printBoard(){
        //外层for循环控制行数
        for(int i = 0; i < 16; i++){
            if(i == 0){
                //第一行打印表头
                System.out.println("    0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f ");
            }
            //内层for循环打印每行内容
            for(int j = 0; j < 16; j ++){
                //每一行第一列打印对应行号
                if (j == 0 && i < 10){
                    System.out.print(" " + i + " ");
                }
                //第十行开始 打印abcdef
                if(j == 0 && i >= 10){
                    switch(i){
                        case 10: System.out.print(" a "); break;
                        case 11: System.out.print(" b "); break;
                        case 12: System.out.print(" c "); break;
                        case 13: System.out.print(" d "); break;
                        case 14: System.out.print(" e "); break;
                        case 15: System.out.print(" f "); break;
                    }
                }
                System.out.print(" + ");
            }
            System.out.println();
        }
    }

    public static void main(String[] args){

//        1 判断用户输入的年月日数据是一年中的第几天并打印
//        getManyDays();
//        2 编程找出 1000 以内的所有完数并打印出来
//        getPerfectNum();
//        3 实现双色球抽奖游戏中奖号码的生成
//        getDoubleColorNum();
//        4 编程题 自定义数组扩容规则
//        customArrayRule();
//        5 编程题 使用二维数组和循环实现五子棋游戏棋盘
//        printBoard();

    }

}
