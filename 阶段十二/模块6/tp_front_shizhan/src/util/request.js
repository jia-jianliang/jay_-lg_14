import axios from 'axios'

const request = axios.create({
  timeout: 1000,
  baseURL: '/api/api'
})
export default request
