import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: window.localStorage.getItem('user') || null,
    username: null,
    projectId: window.localStorage.getItem('user') || null
  },
  getters: {
  },
  mutations: {
    setUser (state, payload) {
      state.user = payload
      window.localStorage.setItem('user', payload)
    },
    saveUser (state, payload) {
      state.username = payload
      window.localStorage.setItem('username', payload)
    },
    saveProjectId (state, payload) {
      state.projectId = payload
      window.localStorage.setItem('projectId', payload)
    }
  },
  actions: {
  },
  modules: {
  }
})
