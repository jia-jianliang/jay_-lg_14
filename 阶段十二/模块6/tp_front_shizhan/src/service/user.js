import request from '@/util/request'

export const login = (data) => {
  return request({
    method: 'POST',
    url: '/login',
    data
  })
}
export const getUserInfo = (data) => {
  return request({
    method: 'GET',
    url: '/user',
    params: {
      username: data
    }
  })
}
