import request from '@/util/request'

export const getProjectList = () => {
  return request({
    method: 'GET',
    url: '/project',
    params: {
      query_type: 'list',
      page: 1,
      size: 10
    }
  })
}
export const addProject = (data) => {
  return request({
    method: 'POST',
    url: '/project',
    data
  })
}
export const deleteProject = (data) => {
  return request({
    method: 'DELETE',
    url: '/project',
    data
  })
}
export const editProject = (data) => {
  return request({
    method: 'PUT',
    url: '/project',
    data
  })
}
