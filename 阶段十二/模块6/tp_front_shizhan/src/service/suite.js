import request from '@/util/request'

export const getSuiteList = (data) => {
  return request({
    method: 'GET',
    url: '/suite',
    params: {
      ...data
    }
  })
}
export const deleteSuite = (data) => {
  return request({
    method: 'DELETE',
    url: '/suite',
    data
  })
}
export const addSuite = (data) => {
  return request({
    method: 'POST',
    url: '/suite',
    data
  })
}
export const editSuite = (data) => {
  return request({
    method: 'PUT',
    url: '/suite',
    data
  })
}
