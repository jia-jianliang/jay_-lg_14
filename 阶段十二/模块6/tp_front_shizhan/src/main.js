import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
// import 'element-ui/lib/theme-chalk/index.css'
import './styles/index.scss'
import '@/styles/global.css'
import '@/assets/font/iconfont.css'
// import axios from 'axios'
// axios.defaults.baseURL = 'http://127.0.0.1:5000'
// axios.defaults.baseURL = '/api'
// Vue.prototype.$http = axios

Vue.use(ElementUI)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
