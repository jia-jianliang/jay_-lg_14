import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: "login" */'@/views/login')
  },
  {
    path: '/',
    component: () => import(/* webpackChunkName: "layout" */'@/views/layout'),
    meta: { requiresAuth: true },
    children: [
      {
        path: '',
        name: 'home',
        component: () => import(/* webpackChunkName: "home" */'@/views/home')
      },
      {
        path: 'member',
        name: 'member',
        component: () => import(/* webpackChunkName: "member" */'@/views/member')
      },
      {
        path: '/project',
        name: 'project',
        component: () => import(/* webpackChunkName: "project" */'@/views/project')
      },
      {
        path: '/suite',
        component: () => import(/* webpackChunkName: "suite" */'@/views/suite'),
        children: [
          {
            path: 'caseList',
            name: 'caseList',
            component: () => import(/* webpackChunkName: "caseList" */'@/views/suite/caseList')
          },
          {
            path: '',
            name: 'testSuite',
            component: () => import(/* webpackChunkName: "testSuite" */'@/views/suite/testSuite')
          }
        ]
      }
    ]
  },
  {
    path: '*',
    name: 'errorPage',
    component: () => import(/* webpackChunkName: "errorPage" */'@/views/errorPage')
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    console.log('需要登录')
    if (!store.state.user) {
      return next({
        name: 'login',
        query: {
          redirect: to.fullPath
        }
      })
    }
    next()
  } else {
    console.log('不需要登录')
    next()
  }
})

export default router
