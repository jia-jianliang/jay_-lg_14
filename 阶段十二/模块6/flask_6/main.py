import json
from flask import Flask, request
from flask_restful import Resource, Api
import logging
from db import db
# 实例化flask对象

app = Flask(__name__)
db.init_app(app)
app.config["SQLALCHEMY_DATABASE_URI"] = "mysql+pymysql://root:123456@127.0.0.1:3306/flask_6?charset=utf8"
with app.app_context():
    db.create_all()

api = Api(app)

# 初始化日志对象,并设置日志等级为：DEBUG级别
logging.basicConfig(level=logging.DEBUG)


# 所有测试用例列表
testcases = []
# 定义一个TestCase列表的对象方法，用来存储testcases对象：testcases[TestCaseList对象1，TestCaseList对象2，TestCaseList对象3] 使用时：TestCaseList对象3.as_dict()可以返回dict数据
class TestCaseList:
    def __init__(self, name, steps, predict):
        self.name = name
        self.steps = steps
        self.description = predict

    # 定义一个返回字典数据的方法
    def as_dict(self):
        return {"name": self.name, "description": self.description, "steps": self.steps}

    # 继承flask_restfule的Resouce类
class TestCaseServer(Resource):
    def __init__(self):
        pass
        # 如果是Get请求，那么是查询所有数据
    def get(self):
        app.logger.info("查询出来的所有testcases数据：", testcases)
        return testcases
    # 如果是post请求，那么新增
    def post(self):
        app.logger.info(request.json)
        try:
            testcase = TestCaseList(**(request.json)) # 把flask的request接受的数据json化，然后通过**解包json数据
            testcases.append(testcase.as_dict()) # 添加testcase的字典数据到testcases中
            return {
            "error": 0,
            "msg": "ok"
            }
        except:
            return {
            "error": 500,
            "msg": "server is error"
            }
        # TestCaseServer到flask_restful的api中去


api.add_resource(TestCaseServer, '/testcase')


if __name__ == '__main__':
    app.run(debug=True, use_reloader=True)