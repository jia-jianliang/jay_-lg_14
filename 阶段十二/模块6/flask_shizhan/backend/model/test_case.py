import datetime

import threadpool

from backend.app import db
from backend.model.test_report import TestReport
from backend.model.test_suite import TestSuite
from backend.util import httper
from backend.util.random_str import random_str


class TestCase(db.Model):
    response_list = []

    __tablename__ = 'test_case'

    id = db.Column(db.INTEGER, autoincrement=True)
    case_name = db.Column(db.String(100),
                          nullable=False)
    method = db.Column(db.String(20), nullable=False)
    # 接口的请求方法
    protocal = db.Column(db.String(20),
                         default="HTTP", nullable=False)  # 协议
    host = db.Column(db.String(50), nullable=False)  # 域名
    port = db.Column(db.INTEGER, nullable=True)  # 端口
    path = db.Column(db.String(100), default='/',
                     nullable=True)  # 资源路径
    params = db.Column(db.String(100), nullable=True)
    # 查询参数
    headers = db.Column(db.String(500), nullable=True)
    # 请求头
    body = db.Column(db.String(500), nullable=True)  # 请求体
    description = db.Column(db.String(500),
                            nullable=True)  # 描述信息
    predict = db.Column(db.String(500), nullable=True)
    # 预期数据
    suite_id = db.Column(db.INTEGER,
                         db.ForeignKey('test_suite.id'), nullable=True)  # 外键关联测试计划的id
    suite = db.relationship('TestSuite',
                            backref=db.backref('test_case', lazy='dynamic'))  # 关联测试计划
    isDeleted = db.Column(db.String(1), default='0')
    status = db.Column(db.String(1), default='1',
                       nullable=True)
    create_time = db.Column(db.DateTime,
                            default=datetime.datetime.now(), nullable=True)
    update_time = db.Column(db.DateTime,
                            nullable=True)
    operation = db.Column(db.String(20),
                          nullable=True)
    db.PrimaryKeyConstraint(id)

    def __repr__(self):
        return f"<TestCase>{self.case_name}"

    @classmethod
    def run_case(cls, json):
        suite_id = json.get("suite_id")
        case = TestCase.query.filter_by(suite_id=suite_id).first()
        if not case or not case.suite or not case.suite.project:
            return None
        case_id_list = json.get("case_list")
        cls.multi_thread(3, case_id_list)
        str_random = random_str()
        pass_count = 0
        failed_count = 0
        test_count = len(cls.response_list)
        total_time = 0
        for response in cls.response_list:
            total_time += int(response.get("overtime"))
            if response.get("assert_result"):
                pass_count = pass_count+1
            else:
                failed_count  = failed_count + 1
        conclusion = "通过" if not failed_count else "不通过"
        report_summeries = {
            "report_name": str_random,
            "report_id": str_random,
            "project_name": json.get("project_name"),
            "project_id": json.get("project_id"),
            "test_count": test_count,
            "pass_count": pass_count,
            "failed_count": failed_count,
            "pass_rate": pass_count/test_count,
            "conclusion": conclusion,
            "total_time": total_time,
            "com_from": json.get("execution_mode"),
            "create_time": str(datetime.datetime.now()),
            "case_data": str(cls.response_list),
        }
        db.session.add(TestReport(**report_summeries))
        db.session.commit()
        db.session.close()
        return report_summeries

    @classmethod
    def run_suite(cls, json):
        suite_id_list = json.get("suite_list")
        for suiteid in suite_id_list:
            suite = TestSuite.query.filter_by(id=suiteid).first()
            cases_of_suite = suite.test_case
            case_id_list = []
            for case in cases_of_suite:
                case_id_list.append(case.id)
            cls.multi_thread(3, case_id_list)
        str_random = random_str()
        pass_count = 0
        failed_count = 0
        test_count = len(cls.response_list)
        total_time = 0
        for response in cls.response_list:
            total_time += int(response.get("overtime"))
            if response.get("assert_result"):
                pass_count = pass_count + 1
            else:
                failed_count = failed_count + 1
        conclusion = "通过" if not failed_count else "不通过"
        report_summeries = {
            "report_name": str_random,
            "report_id": str_random,
            "project_name": json.get("project_name"),
            "project_id": json.get("project_id"),
            "test_count": test_count,
            "pass_count": pass_count,
            "failed_count": failed_count,
            "pass_rate": pass_count / test_count,
            "conclusion": conclusion,
            "total_time": total_time,
            "com_from": json.get("execution_mode"),
            "create_time": str(datetime.datetime.now()),
            "case_data": str(cls.response_list),
        }
        db.session.add(TestReport(**report_summeries))
        db.session.commit()
        db.session.close()
        return report_summeries

    @classmethod
    def _run_case(cls, caseid):
        _case = TestCase.query.filter_by(id=caseid).first().to_dict()
        response = httper.send(_case)
        return response

    @classmethod
    def collection_result(cls, *args):
        cls.response_list.append(args[1])

    @classmethod
    def multi_thread(cls, poolsize, caseid_list):
        pool = threadpool.ThreadPool(poolsize)
        task_list = threadpool.makeRequests(cls._run_case, caseid_list, callback=cls.collection_result)
        [pool.putRequest(task) for task in task_list]
        pool.wait()

    @classmethod
    def add_case(cls, json):
        suite = TestSuite.query.filter_by(id=json.get("suite_id")).first()
        if not suite:
            return False
        if suite.project.isDeleted == "1":
            return False
        db.session.add(TestCase(**json))
        db.session.commit()
        db.session.close()
        return True

    @classmethod
    def get_by_id(cls, id):
        case = TestCase.query.filter_by(id=id).first()
        return case.to_dict() if case else None

    @classmethod
    def get_by_name(cls, name):
        cases = TestCase.query.filter_by(case_name=name).all()
        if not cases:
            return None
        result_list = []
        for case in cases:
            result_list.append(case.to_dict())
        return result_list

    @classmethod
    def get_list(cls, suite_id, page=1, size=10):
        case = TestCase.query.filter_by(suite_id=suite_id).first()
        if not case or not case.suite or not case.suite.project:
            return None
        cases = TestCase.query.filter_by(suite_id=suite_id, isDeleted="0").slice((page - 1) * size, page * size).all()
        if not cases:
            return None
        result_list = []
        for case in cases:
            result_list.append(case.to_dict())
        return result_list

    @classmethod
    def modify_case(cls, json):
        case = TestCase.query.filter_by(id=json.get("id")).first()
        if not case or not case.suite or not case.suite.project:
            return False
        cls._modify_case(case, json)
        db.session.commit()
        db.session.close()
        return True

    @classmethod
    def delete_case(cls, id):
        case = TestCase.query.filter_by(id=id).first()
        if not case:
            return False
        case.isDeleted = "1"
        db.session.commit()
        db.session.close()
        return True

    @classmethod
    def _modify_case(cls, case, json):
        case.case_name = json.get("case_name", None) or case.case_name
        case.method = json.get("method", None) or case.method
        case.protocal = json.get("protocal", None) or case.protocal
        case.host = json.get("host", None) or case.host
        case.port = json.get("port", None) or case.port
        case.path = json.get("path", None) or case.path
        case.params = json.get("params", None) or case.params
        case.headers = json.get("headers", None) or case.headers
        case.body = json.get("body", None) or case.body
        case.description = json.get("description", None) or case.description
        case.predict = json.get("predict", None) or case.predict
        return case

    def to_dict(self):
        return {
            "id": self.id,
            "case_name": self.case_name,
            "method": self.method,
            "protocal": self.protocal,
            "host": self.host,
            "port": self.port,
            "path": self.path,
            "params": self.params,
            "headers": self.headers,
            "body": self.body,
            "description": self.description,
            "predict": self.predict,
            "suite_id": self.suite_id,
            "isDeleted": self.isDeleted,
            "status": self.status,
            "create_time": str(self.create_time),
            "update_time": str(self.update_time),
            "operation": self.operation
        }
