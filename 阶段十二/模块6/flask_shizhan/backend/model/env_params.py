import datetime
from backend.app import db


class EnvParams(db.Model):
    __tablename__ = 'env_params'  # env_params

    id = db.Column(db.INTEGER, autoincrement=True)  # 环境变量表的id
    params_key = db.Column(db.String(100), nullable=False)  # 变量名
    params_value = db.Column(db.String(100), nullable=False)  # 变量值
    description = db.Column(db.String(500), nullable=True)  # 环境的描述
    env_id = db.Column(db.INTEGER, db.ForeignKey('env.id'), nullable=True)  # 外键关联环境变量的id
    environment = db.relationship('Env', backref=db.backref('env', lazy='dynamic'))  # 关联环境
    isDeleted = db.Column(db.String(1), default='0')
    status = db.Column(db.String(1), default='1', nullable=True)
    create_time = db.Column(db.DateTime, default=datetime.datetime.now(), nullable=True)
    update_time = db.Column(db.DateTime, nullable=True)
    operation = db.Column(db.String(20), nullable=True)
    db.PrimaryKeyConstraint(id)

    def __repr__(self):
        return '<EnvParams> %r' % self.params_key

    def to_dict(self):
        return {
            "id": self.id,
            "params_key": self.params_key,
            "params_value": self.params_value,
            "description": self.description,
            "env_id": self.env_id,
            "environment": self.environment.to_dict(),
            "isDeleted": self.isDeleted,
            "status": self.status,
            "create_time": str(self.create_time),
            "update_time": str(self.update_time),
            "operation": self.operation
        }

    @classmethod
    def get_by_id(cls, id):
        env_params = EnvParams.query.filter_by(id=id).first()
        if not env_params:
            return None
        return env_params.to_dict()

    @classmethod
    def get_by_key(cls, params_key):
        result = EnvParams.query.filter_by(params_key=params_key).all()
        if not result:
            return None
        result_list = []
        for env_params in result:
            result_list.append(env_params.to_dict())
        return result_list

    @classmethod
    def get_list(cls, page=1, size=10):
        all_data = EnvParams.query.filter(EnvParams.isDeleted == 0).slice((page - 1) * size, page * size).all()
        if not all_data:
            return None
        result_list = []
        for env_params in all_data:
            result_list.append(env_params.to_dict())
        return result_list

    @classmethod
    def modify_env_params(cls, env_id, params_id, params_key, params_value, description):
        env_params = EnvParams.query.filter_by(id=params_id, env_id=env_id, isDeleted=0).first()
        if not env_params:
            return None
        env_params.params_key = params_key or env_params.params_key
        env_params.params_value = params_value or env_params.params_value
        env_params.description = description or env_params.description
        db.session.commit()
        db.session.close()

    @classmethod
    def delete_env_params(cls, id):
        project = EnvParams.query.filter_by(id=id).first()
        project.isDeleted = 1
        db.session.commit()
        db.session.close()

    @classmethod
    def add_env_params(cls, json):
        db.session.add(EnvParams(**json))
        db.session.commit()
        db.session.close()