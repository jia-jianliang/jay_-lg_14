import datetime
from backend.app import db


class TestReport(db.Model):

    __tablename__ = 'report_summerise'  # 定义报告的表名是report_summerise
    id = db.Column(db.INTEGER, autoincrement=True)
    report_name = db.Column(db.String(100), nullable=False)
    report_id = db.Column(db.String(100), nullable=False)
    project_id = db.Column(db.INTEGER, autoincrement=True)
    project_name = db.Column(db.String(100), nullable=True)
    test_count = db.Column(db.INTEGER, nullable=True)
    pass_count = db.Column(db.INTEGER, nullable=True)
    failed_count = db.Column(db.INTEGER, nullable=True)
    pass_rate = db.Column(db.String(20), nullable=True)
    conclusion = db.Column(db.String(20), nullable=True)
    total_time = db.Column(db.String(100), nullable=True)
    com_from = db.Column(db.String(100), nullable=True)
    case_data = db.Column(db.JSON, nullable=True)
    create_time = db.Column(db.DateTime, default=datetime.datetime.now(), nullable=True)
    update_time = db.Column(db.DateTime, nullable=True)
    db.PrimaryKeyConstraint(id)

    def __repr__(self):
        return f"<TestReport> {self.report_name}"

    def to_dict(self):
        return {
            "id": self.id,
            "report_name": self.report_name,
            "report_id": self.report_id,
            "project_id": self.project_id,
            "project_name": self.project_name,
            "test_count": self.test_count,
            "pass_count": self.pass_count,
            "failed_count": self.failed_count,
            "pass_rate": self.pass_rate,
            "conclusion": self.conclusion,
            "total_time": self.total_time,
            "com_from": self.com_from,
            "case_data": self.case_data,
            "create_time": str(self.create_time),
            "update_time": str(self.update_time)
        }