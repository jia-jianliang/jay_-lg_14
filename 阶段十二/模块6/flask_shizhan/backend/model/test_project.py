import datetime
import json

from backend.app import db


class TestProject(db.Model):
    __tablename__ = 'test_project'  # 定义表名是
    id = db.Column(db.INTEGER, autoincrement=True)
    project_name = db.Column(db.String(100), nullable=False)
    description = db.Column(db.String(500), nullable=True)
    isDeleted = db.Column(db.String(1), default='0')
    status = db.Column(db.String(1), default='1', nullable=True)
    create_time = db.Column(db.DateTime, default=datetime.datetime.now(), nullable=True)
    update_time = db.Column(db.DateTime, nullable=True)
    operation = db.Column(db.String(20), nullable=True)
    db.PrimaryKeyConstraint(id)

    def to_dict(self):
        return {
            "id": self.id,
            "project_name": self.project_name,
            "description": self.description,
            "isDeleted": self.isDeleted,
            "status": self.status,
            "create_time": str(self.create_time),
            "update_time": str(self.update_time),
            "operation": self.operation,
        }

    def __repr__(self):
        return '<Project> %r' % self.project_name

    # def to_dict(self):
    #     return {tcm.name: getattr(self, tcm.name) for tcm in TestProject.__table__.columns}

    @classmethod
    def get_by_id(cls, id):
        project = TestProject.query.filter_by(id=id).first()
        if not project:
            return None
        return project.to_dict()

    @classmethod
    def get_by_name(cls, name):
        result = TestProject.query.filter_by(project_name=name).all()
        if not result:
            return None
        result_list = []
        for r in result:
            result_list.append(r.to_dict())
        return result_list

    @classmethod
    def get_list(cls, page=1, size=10):
        all_data = TestProject.query.filter(TestProject.isDeleted == 0).slice((page - 1) * size, page * size).all()
        if not all_data:
            return None
        return cls.to_list(all_data)

    @classmethod
    def modify_project(cls, id, project_name, description):
        project = TestProject.query.filter_by(id=id, isDeleted=0).first()
        if not project:
            return None
        project.project_name = project_name or project.project_name
        project.description = description or project.description
        db.session.commit()
        db.session.close()

    @classmethod
    def delete_project(cls, id):
        project = TestProject.query.filter_by(id=id).first()
        project.isDeleted = 1
        db.session.commit()
        db.session.close()


    @classmethod
    def add_project(cls, json):
        db.session.add(TestProject(**json))
        db.session.commit()
        db.session.close()

    @classmethod
    def to_list(cls, data):
        result_list = []
        for d in data:
            result_list.append(d.to_dict())

        return result_list