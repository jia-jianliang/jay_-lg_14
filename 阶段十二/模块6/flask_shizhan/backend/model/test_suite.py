import datetime

from backend.app import db
from backend.model.test_project import TestProject


class TestSuite(db.Model):

    __tablename__ = "test_suite"

    id = db.Column(db.INTEGER, autoincrement=True)
    suite_name = db.Column(db.String(100), nullable=False)
    description = db.Column(db.String(500), nullable=True)
    project_id = db.Column(db.INTEGER, db.ForeignKey('test_project.id'), nullable=False)  # 外键关联测试计划的id
    # project_id = db.Column(db.INTEGER, db.ForeignKey('TestProject.id'), nullable=False)  # 外键关联测试计划的id
    project = db.relationship('TestProject', backref=db.backref('test_suite', lazy='dynamic'))  # 关联测试计划
    isDeleted = db.Column(db.String(1), default='0')
    status = db.Column(db.String(1), default='1', nullable=True)
    create_time = db.Column(db.DateTime, default=datetime.datetime.now(), nullable=True)
    update_time = db.Column(db.DateTime, nullable=True)
    operation = db.Column(db.String(20), nullable=True)
    db.PrimaryKeyConstraint(id)

    @classmethod
    def add_suite(cls, json):
        project_id = int(json.get("project_id"))
        project = TestProject.get_by_id(project_id)
        if not project:
            return False
        data = TestSuite(**json)
        db.session.add(data)
        db.session.commit()
        db.session.close()
        return True

    @classmethod
    def get_by_id(cls, id):
        result = TestSuite.query.filter_by(id=id).first()
        if not result:
            return None
        return result.to_dict()

    @classmethod
    def get_by_name(cls, name):
        result = TestSuite.query.filter_by(suite_name=name).all()
        if not result:
            return None
        result_list = []
        for r in result:
            result_list.append(r.to_dict())
        return result_list

    @classmethod
    def get_list(cls, project_id, page=1, size=10):
        result = TestSuite.query.filter(TestSuite.project_id==project_id, TestSuite.isDeleted=="0").slice((page-1) * size, page*size).all()
        if not result:
            return None
        result_list = []
        for r in result:
            result_list.append(r.to_dict())
        return result_list

    @classmethod
    def modify_suite(cls, suite_id, project_id, suite_name, description):
        suite = TestSuite.query.filter_by(id=suite_id).first()
        if not suite:
            return False
        suite.project_id = project_id
        suite.suite_name = suite_name
        suite.description = description
        db.session.commit()
        db.session.close()
        return True

    @classmethod
    def delete_suite(cls, id):
        suite = TestSuite.query.filter_by(id=id).first()
        if not suite:
            return False
        suite.isDeleted = "1"
        db.session.commit()
        db.session.close()
        return True

    def __repr__(self):
        return f"<TestSuite>{self.suite_name}"

    def to_dict(self):
        return {
            "id": self.id,
            "suite_name": self.suite_name,
            "description": self.description,
            "project_id": self.project_id,
            "isDeleted": self.isDeleted,
            "status": self.status,
            "create_time": str(self.create_time),
            "update_time": str(self.update_time),
            "operation": self.operation
        }
