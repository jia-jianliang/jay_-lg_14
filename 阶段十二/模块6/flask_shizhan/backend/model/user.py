import datetime

from flask_login import UserMixin

from backend.app import db


class User(db.Model, UserMixin):
    __tablename__ = 'user'  # 定义表名是users
    id = db.Column(db.INTEGER, autoincrement=True)  # 主键id
    username = db.Column(db.String(100), nullable=False)  # 用户名
    password = db.Column(db.String(100), nullable=True)  # 密码
    nick_name = db.Column(db.String(100), nullable=True)  # 昵称
    role_id = db.Column(db.String(10), nullable=True)  # 角色
    isDeleted = db.Column(db.String(1), default='0')  # 删除状态
    status = db.Column(db.String(1), default='1', nullable=True)  # 禁用状态
    create_time = db.Column(db.DateTime, default=datetime.datetime.now(), nullable=True)  # 创建    时间
    update_time = db.Column(db.DateTime, nullable=True)  # 修改时间
    db.PrimaryKeyConstraint(id)

    def __repr__(self):
        return '<UserModel> %r' % self.username

    def to_dict(self):
        return {
            "id": self.id,
            "username": self.username,
            "password": self.password,
            "nick_name": self.nick_name,
            "role_id": self.role_id,
            "isDeleted": self.isDeleted,
            "status": self.status,
            "create_time": str(self.create_time),
            "update_time": str(self.update_time),
        }

    @classmethod
    def add_user(cls, request_json):
        db.session.add(User(**request_json))
        db.session.commit()
        db.session.close()

    @classmethod
    def query_user(cls, username):
        user = User.query.filter_by(username=username).first()
        if not user:
            return {}
        return user.to_dict()

    @classmethod
    def get_by_id(cls, id):
        user = User.query.filter_by(id=id).first()
        return user.to_dict()

    @classmethod
    def modify_user(cls, request_json):
        id = request_json.get("id")
        user = User.query.filter_by(id=id).first()
        if not user:
            return None
        user.password = request_json.get("password")
        user.nick_name = request_json.get("nick_name")
        db.session.commit()
        db.session.close()

    @classmethod
    def delete_user(cls, id):
        user = User.query.filter_by(id=id).first()
        if not user:
            return None
        user.isDeleted = "1"
        db.session.commit()
        db.session.close()
