from backend.app import db


class CronJob(db.Model):
    __tablename__ = 'cron_job'  # 设置表名
    # 字段
    id = db.Column(db.String(191), unique=True, primary_key=True)
    next_run_time = db.Column(db.Float(25), unique=False, nullable=True)
    job_state = db.Column(db.BLOB, unique=False, nullable=True)
    cron_name = db.Column(db.String(20))

    def __repr__(self):
        return f"<CronJob> {self.cron_name}"

    @classmethod
    def modify_name(self, id, cron_name):
        data = {
            "cron_name": cron_name
        }
        CronJob.query.filter_by(id=id).update(data)
        db.session.commit()
        db.session.close()
        return True
