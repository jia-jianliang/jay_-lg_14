import datetime
from backend.app import db


class Env(db.Model):
    __tablename__ = 'env'  # 定义表名是env

    id = db.Column(db.INTEGER, autoincrement=True)  # 环境的id
    env_name = db.Column(db.String(100), nullable=False)  # 环境的名称
    description = db.Column(db.String(500), nullable=True)  # 环境的描述
    isDeleted = db.Column(db.String(1), default='0')
    status = db.Column(db.String(1), default='1', nullable=True)
    create_time = db.Column(db.DateTime, default=datetime.datetime.now(), nullable=True)
    update_time = db.Column(db.DateTime, nullable=True)
    operation = db.Column(db.String(20), nullable=True)
    db.PrimaryKeyConstraint(id)

    def __repr__(self):
        return '<Env> %r' % self.env_name

    def to_dict(self):
        return {
            "id": self.id,
            "env_name": self.env_name,
            "description": self.description,
            "isDeleted": self.isDeleted,
            "status": self.status,
            "create_time": str(self.create_time),
            "update_time": str(self.update_time),
            "operation": self.operation
        }

    @classmethod
    def get_by_id(cls, id):
        env = Env.query.filter_by(id=id).first()
        if not env:
            return None
        return env.to_dict()

    @classmethod
    def get_by_name(cls, env_name):
        result = Env.query.filter_by(env_name=env_name).all()
        if not result:
            return None
        result_list = []
        for env in result:
            result_list.append(env.to_dict())
        return result_list

    @classmethod
    def get_list(cls, page=1, size=10):
        all_data = Env.query.filter(Env.isDeleted == 0).slice((page - 1) * size, page * size).all()
        if not all_data:
            return None
        result_list = []
        for env in all_data:
            result_list.append(env.to_dict())
        return result_list

    @classmethod
    def modify_env(cls, id, env_name, description):
        env = Env.query.filter_by(id=id, isDeleted=0).first()
        if not env:
            return None
        env.env_name = env_name or env.env_name
        env.description = description or env.description
        db.session.commit()
        db.session.close()

    @classmethod
    def delete_env(cls, id):
        project = Env.query.filter_by(id=id).first()
        project.isDeleted = 1
        db.session.commit()
        db.session.close()

    @classmethod
    def add_env(cls, json):
        db.session.add(Env(**json))
        db.session.commit()
        db.session.close()
