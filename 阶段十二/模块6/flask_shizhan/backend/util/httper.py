import json

import requests
from requests.adapters import HTTPAdapter

session = requests.Session()
session.mount("http://", HTTPAdapter(max_retries=3)) # 配置超时重试次数
session.mount("https://", HTTPAdapter(max_retries=3))

def send(request_json):
    protocal = request_json.get("protocal")
    host = request_json.get("host")
    port = request_json.get("port")
    path = request_json.get("path")
    url = protocal + "://" + host + ":" + str(port) + path
    method = request_json.get("method")
    params = request_json.get("params")
    if params:
        params = json.loads(params.replace("\'", "\""))
    json_data = request_json.get("body")
    if json_data:
        json_data = json.loads(json_data.replace("\'", "\""))
    headers = request_json.get("headers")
    if headers:
        headers = json.loads(headers.replace("\'", "\""))
    if "get" == method.lower():
        response = session.request(method=method.upper(), url=url, params=params, headers=headers)
    else:
        response = session.request(method=method, url=url, params=params, headers=headers, json=json_data)
    ass_result = assert_result(predict=request_json.get("predict"), response=response)
    report_detail = {
        "case_id": request_json.get("id"),
        "case_name": request_json.get("case_name"),
        "suite_id": request_json.get("suite_id"),
        "suite_name": request_json.get("suite_id"),
        "methods": request_json.get("method"),
        "url": response.url,
        "headers": request_json.get("headers"),
        "body": request_json.get("body"),
        "response_data": response.json(),
        "status_code": response.status_code,
        "predict": request_json.get("predict"),
        "assert_result": ass_result,
        "test_result": "成功" if ass_result else "失败",
        "overtime": response.elapsed.seconds,
    }
    return report_detail

def assert_result(predict, response):
    return True
