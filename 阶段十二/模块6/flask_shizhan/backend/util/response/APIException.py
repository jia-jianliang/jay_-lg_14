from werkzeug.exceptions import HTTPException

from backend.util.response.code import Code
from backend.util.response.result import Result


class APIException(HTTPException):
    code = 500
    message = "Server Interal Error!"

    def __init__(self, code=None, message=None):
        if code:
            self.code = code
        self.message = message if message else Code.MSG.get(code)
        super(APIException, self).__init__(self.message, None)

    def get_body(self, environ=None, *args, **kwargs):
        return Result.fail(self.code)

    def get_headers(self, environ, scope):
        return [("Content-Type", "application/json")]


class NotFound(APIException):
    code = 404
    message = "未找到页面"
