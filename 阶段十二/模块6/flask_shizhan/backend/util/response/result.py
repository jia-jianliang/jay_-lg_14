import json

from backend.util.response.code import Code


class Result:
    def __init__(self, code, msg, data):
        self.code = code
        self.msg = msg
        self.data = data

    @classmethod
    def success(cls, data=None):
        return {"code": 0, "msg": "ok", "data": data}

    @classmethod
    def fail(cls, code=-1):
        return {"code": code, "msg": Code.MSG.get(code)}
