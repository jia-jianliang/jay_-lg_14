class Code():

    SUCCESS = 1
    FAIL = -1
    NOT_FOUND = 404
    UNKONW_ERROR = 500
    MISS_PARAM = 400
    VALUE_CANNOT_BE_NULL = 999
    MUST_JSON_DATA = 888
    FAIL_TO_ADD = 666


    MSG = {
        SUCCESS: "成功",
        FAIL: "失败",
        NOT_FOUND: "not found!!!",
        UNKONW_ERROR: "服务器内部错误，请联系开发人员！",
        MISS_PARAM: "缺少参数",
        VALUE_CANNOT_BE_NULL: "value不能为空！",
        MUST_JSON_DATA: "必须携带json数据",
        FAIL_TO_ADD: "添加失败"
    }