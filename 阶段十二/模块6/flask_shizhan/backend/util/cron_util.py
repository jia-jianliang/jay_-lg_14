# -*- encoding=utf-8 -*-
import pickle

from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.interval import IntervalTrigger
from pytz import timezone

from backend.app import app
from backend.config import USERNAME, PASSWORD, HOST, PORT, DATABASE
from backend.model.cron_job import CronJob
from backend.model.test_case import TestCase


class CronUtil:
    # The "apscheduler." prefix is hard coded
    scheduler = BackgroundScheduler({
        'apscheduler.jobstores.default': {
            'type': 'sqlalchemy',
            'url': f'mysql+pymysql://{USERNAME}:{PASSWORD}@{HOST}:{PORT}/{DATABASE}?charset=utf8',
            "tablename": "cron_job"
        },
        'apscheduler.executors.default': {
            'class': 'apscheduler.executors.pool:ThreadPoolExecutor',
            'max_workers': '20'
        },
        'apscheduler.executors.processpool': {
            'type': 'processpool',
            'max_workers': '5'
        },
        'apscheduler.job_defaults.coalesce': 'false',
        'apscheduler.job_defaults.max_instances': '3',
        'apscheduler.timezone': timezone("Asia/Shanghai"),
    })

    @classmethod
    def add_job(cls, func, json_data):
        cron_type = json_data.get("cron_tab").get("cron_type")
        args_data = {
            "project_id": json_data.get("project_id"),
            "execution_mode": json_data.get("execution_mode"),
            "project_name": json_data.get("project_name"),
            "suite_list": json_data.get("suite_id_list"),
        }
        seconds = json_data.get("cron_tab").get("interval_list").get("seconds")
        if cron_type == "date":
            pass
        if cron_type == "cron":
            pass
        if cron_type == "interval":
            job = cls.scheduler.add_job(func, trigger="interval",
                                        seconds=int(seconds), args=(args_data,),
                                        replace_existing=True, coalesce=True)
        cron_name = json_data.get("cron_name", None)
        if cron_name:
            CronJob.modify_name(job.id, cron_name)
        return job

    @classmethod
    def modify_job(cls, json_data):
        cron_id = json_data.get("cron_id")
        seconds = json_data.get("cron_tab").get("interval_list").get("seconds")
        modify_data = {
            "project_id": json_data.get("project_id"),
            "project_name": json_data.get("project_name"),
            "suite_id_list": json_data.get("suite_id_list"),
            "execution_mode": json_data.get("execution_mode"),
        }
        job = cls.scheduler.modify_job(job_id=cron_id,
                                       trigger=IntervalTrigger(seconds=int(seconds)),
                                       args=(modify_data,)
                                       )
        cron_name = json_data.get("cron_name", None)
        if cron_name:
            CronJob.modify_name(cron_id, cron_name)
        return job

    @classmethod
    def get_by_id(cls, id):
        cron_job = CronJob.query.filter_by(id=id).first()
        # print(cls._to_response(cron_job))
        return cls._to_response(cron_job)

    @classmethod
    def get_list(cls, page, size):
        cron_jobs = CronJob.query.filter().slice((page - 1) * size, page * size).all()
        if not cron_jobs:
            return None
        response_list = []
        for cron_job in cron_jobs:
            response_list.append(cls._to_response(cron_job))
        return response_list

    @classmethod
    def get_by_name(cls, cron_name):
        cron_jobs = CronJob.query.filter_by(cron_name=cron_name).all()
        if not cron_jobs:
            return None
        response_list = []
        for cron_job in cron_jobs:
            response_list.append(cls._to_response(cron_job))
        return response_list

    # 开始定时任务
    @classmethod
    def start_cron(cls):
        cls.scheduler.start()

    # 暂停定时任务
    @classmethod
    def pause_cron(cls, cron_id=None):
        if not cron_id:
            cls.scheduler.pause() # 暂停所有
        else:
            cls.scheduler.pause_job(cron_id) # 暂停制定job
    # 恢复定时任务
    @classmethod
    def resume_cron(cls, job_id=None):
        # if not job_id:
        #     cls.scheduler.resume() # 恢复全部任务
        # else:
        #     cls.scheduler.resume_job(job_id)
        cls.scheduler.resume_job(job_id)

    # 关闭定时任务
    @classmethod
    def shutdown(cls, wait=True):
        cls.scheduler.shutdown(wait) # wait=True表示等待任务执行完毕以后再关闭

    # 删除定时任务
    @classmethod
    def delete_cron(cls, job_id=None):
        if not job_id:
            cls.scheduler.remove_all_jobs()
        else:
            cls.scheduler.remove_job(job_id)


    @classmethod
    def _to_response(self, cron_job):

        job_state = pickle.loads(cron_job.job_state)
        args = job_state.get("args")[0]
        trigger = job_state.get("trigger")
        seconds = int(str(trigger.interval).split(":")[-2])*60
        start_date = str(trigger.start_date)
        time_zone = trigger.timezone.zone
        app.logger.info(job_state)
        app.logger.info(cron_job.cron_name)
        response = {
            "status": "1",
            "message": "查询单个定时任务成功",
            "data": {
                "cron_name": cron_job.cron_name,
                "project_id": args.get("project_id"),
                "project_name": args.get("project_name"),
                "env_id": "1",
                "suite_id_list": args.get("suite_id_list"),
                "execution_mode": args.get("execution_mode"),
                "trigger": {
                    "interval": seconds,
                    "start_date": start_date,
                    "timezone": time_zone
                },
            "next_run_time": cron_job.next_run_time
            }
        }
        return response


if __name__ == "__main__":
    # CrontUtil.get_by_id("12c1d596a876474ea64efc4f74b87e13")
    # print(CrontUtil.get_list(1, 10))
    print(CronUtil.get_by_name("定时任务rename"))
    # modify_data = {
    #     "cron_id": "12c1d596a876474ea64efc4f74b87e13",
    #     "project_id": "1",
    #     "project_name": "拉勾商城",
    #     "cron_name": "定时任务rename",
    #     "env_id": "1",
    #     "suite_id_list": "1",
    #     "execution_mode": "定时任务执行用例",
    #     "cron_tab": {
    #         "cron_type": "interval",
    #         "collection_list": "定时任务",
    #         "interval_list": {
    #             "weeks": "",
    #             "days": "",
    #             "hours": "",
    #             "minutes": "",
    #             "seconds": "120",
    #             "start_date": "",
    #             "end_date": "",
    #             "timezone": ""
    #         },
    #         "date_list": {
    #             "run_date": "2021-11-23 16:00:00",
    #             "timezone": "UTC-8"
    #         },
    #         "cron_list": {
    #             "year": "",
    #             "month": "",
    #             "day": "",
    #             "week": "",
    #             "day_of_week": "",
    #             "hour": "",
    #             "minute": "",
    #             "second": "",
    #             "start_date": "",
    #             "end_date": "",
    #             "timezone": ""
    #         }
    #     }
    # }
    # CrontUtil.scheduler.start()
    #
    # CrontUtil.modify_job(modify_data)
    #
    # json_data = {
    #     "project_id": "1",
    #     "project_name": "拉勾商城",
    #     "env_id": "1",
    #     "cron_name": "定时任务",
    #     "suite_id_list": "1",
    #     "execution_mode": "定时任务执行用例",
    #     "cron_tab": {
    #         "cron_type": "interval",
    #         "interval_list": {
    #             "weeks": "",
    #             "days": "",
    #             "hours": "",
    #             "minutes": "",
    #             "seconds": "3600",
    #             "start_date": "",
    #             "end_date": "",
    #             "timezone": ""
    #         },
    #         "date_list": {
    #             "run_date": "2021-11-23 16:00:00",
    #             "timezone": "UTC-8"
    #         },
    #         "cron_list": {
    #             "year": "",
    #             "month": "",
    #             "day": "",
    #             "week": "",
    #             "day_of_week": "",
    #             "hour": "",
    #             "minute": "",
    #             "second": "",
    #             "start_date": "",
    #             "end_date": "",
    #             "timezone": ""
    #         }
    #     }
    # }
    # CrontUtil.scheduler.start()
    # CrontUtil.add_job(TestCase.run_suite, json_data)
