import logging
import os.path
from logging import handlers
from backend.config import LOG_LEVEL
from backend.config import BASE_DIR


# 定义一个收集日志的方法
def init_logging(logger_name=None, stream=None):
    # 实例化日志器
    logger = logging.getLogger(logger_name)
    # 设置日志等级
    logger.setLevel(LOG_LEVEL)
    # 设置处理器
    sh = logging.StreamHandler(stream=stream)  # 设置控制台处理器
    fh = logging.handlers.TimedRotatingFileHandler(
        filename=BASE_DIR + os.sep + "logs/lagou.log",
        when='h',
        interval=24,
        backupCount=7,
        encoding="utf-8"
    )
    # 设置格式化器
    fmt = "[%(asctime)s %(levelname)s %(threadName)s%(filename)s %(funcName)s %(lineno)d] [%(thread)d] [%(message)s]"
    formatter = logging.Formatter(fmt)
    # 将格式化器添加到处理器
    fh.setFormatter(formatter)
    sh.setFormatter(formatter)
    # 将处理器添加到日志器
    logger.addHandler(fh)
    logger.addHandler(sh)
    return logger
