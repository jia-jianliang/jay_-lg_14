# 生成测试报告的id
import random
import string
import time


def random_str(randomlength=32):
    str_list = random.sample(string.digits + string.ascii_letters, randomlength - 13)
    result = ''.join(str_list)
    result = str(int(time.time() * 1000)) + result
    return result


print(random_str())
