from backend.app import db, api, app
from backend.controller.project import Project
from backend.controller.suite import Suite
from backend.controller.case import Case
from backend.controller.run_case import RunCase
from backend.controller.cron_job import CronJob
from backend.controller.cron_job import CronJob
from backend.controller.env import Env
from backend.controller.env_params import EnvParams
from backend.controller.user import User, Login, Logout
from backend.model.test_project import TestProject
from backend.model.test_suite import TestSuite
from backend.model.test_case import TestCase
from backend.model.test_report import TestReport
from backend.model.env import Env as EnvModel
from backend.model.env_params import EnvParams as EnvParamsModel
from backend.model.user import User as UserModel


if __name__ == '__main__':
    # db.drop_all()
    db.create_all()
    api.add_resource(Project, "/api/project")
    api.add_resource(Suite, "/api/suite")
    api.add_resource(Case, "/api/case")
    api.add_resource(RunCase, "/api/run/testcase")
    api.add_resource(CronJob, "/api/cron")
    api.add_resource(Env, "/api/env")
    api.add_resource(EnvParams, "/api/env/params")
    api.add_resource(User, "/api/user")
    api.add_resource(Login, "/api/login")
    api.add_resource(Logout, "/api/logout")
    app.run(debug=True)