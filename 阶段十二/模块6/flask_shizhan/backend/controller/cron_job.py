from flask import request, logging
from flask_restful import Resource

from backend.app import app
from backend.model.test_case import TestCase
from backend.util.cron_util import CronUtil
from backend.util.response.APIException import APIException
from backend.util.response.code import Code
from backend.util.response.result import Result


class CronJob(Resource):
    def get(self):
        query_type_dict = {
            "id": CronUtil.get_by_id,
            "name": CronUtil.get_by_name,
            "list": CronUtil.get_list
        }
        query_data = request.args.to_dict()
        if not query_data:
            raise APIException(Code.MISS_PARAM)
        if not query_data.get("query_type", None):
            raise APIException(Code.MISS_PARAM)
        query_type = query_data.pop("query_type")
        if not query_type:
            raise APIException(Code.MISS_PARAM)
        if query_type not in query_type_dict:
            raise APIException(Code.MISS_PARAM)
        if query_type != "list":
            if not query_data.get(query_type):
                raise APIException(Code.VALUE_CANNOT_BE_NULL)
        model_func = query_type_dict.get(query_type)
        if query_type == "list":
            result = model_func(int(query_data.pop("page")), int(query_data.pop("size")))
        else:
            result = model_func(query_data.pop(query_type))
        return Result.success(data=result)

    def post(self):
        request_json = request.get_json()
        if not request_json:
            raise APIException(Code.MISS_PARAM)
        job = CronUtil.add_job(TestCase.run_suite, request_json)
        app.logger.info("________________________________________")
        app.logger.info(job)
        return Result.success(data=request_json)

    def put(self):
        request_json = request.get_json()
        if not request_json:
            raise APIException(Code.MUST_JSON_DATA)
        id = request_json.get("cron_id", None)
        if not id:
            raise APIException(Code.MISS_PARAM)
        CronUtil.modify_job(request_json)
        return Result.success(request_json)

    def delete(self):
        request_json = request.get_json()
        if not request_json:
            raise APIException(Code.MUST_JSON_DATA)
        cron_id = request_json.get("cron_id", None)
        if not cron_id:
            raise APIException(Code.MISS_PARAM)
        CronUtil.delete_cron(cron_id)
        return Result.success()

    # 禁用、启用、恢复等操作
    def patch(self):
        request_json = request.get_json()
        if not request_json:
            raise APIException(Code.MUST_JSON_DATA)
        action = request_json.get("action", None)
        # if not action:
        #     raise APIException(Code.MISS_PARAM)
        if action == 0:
            cron_id = request_json.get("cron_id")
            CronUtil.resume_cron(cron_id)
        if action == "1":
            cron_id = request_json.get("cron_id")
            CronUtil.resume_cron(cron_id)
        if action == "2":
            cron_id = request_json.get("cron_id")
            CronUtil.pause_cron(cron_id)
        if action == "3":
            CronUtil.pause_cron(cron_id)
        return Result.success()