from flask import request
from flask_restful import Resource

from backend.util.response.APIException import APIException
from backend.util.response.code import Code
from backend.model.env import Env as EnvModel
from backend.util.response.result import Result


class Env(Resource):
    def get(self):
        query_type_dict = {
            "id": EnvModel.get_by_id,
            "name": EnvModel.get_by_name,
            "list": EnvModel.get_list
        }
        query_data = request.args.to_dict()
        if not query_data:
            raise APIException(Code.MISS_PARAM)
        if not query_data.get("query_type", None):
            raise APIException(Code.MISS_PARAM)
        query_type = query_data.pop("query_type")
        if not query_type:
            raise APIException(Code.MISS_PARAM)
        if query_type not in query_type_dict:
            raise APIException(Code.MISS_PARAM)
        if query_type != "list":
            if not query_data.get(query_type):
                raise APIException(Code.VALUE_CANNOT_BE_NULL)
        model_func = query_type_dict.get(query_type)
        if query_type == "list":
            result = model_func(int(query_data.pop("page")), int(query_data.pop("size")))
        else:
            result = model_func(query_data.pop(query_type))
        return Result.success(data=result)

    def post(self):
        request_json = request.get_json()
        if not request_json:
            raise APIException(Code.MISS_PARAM)
        if "env_name" not in request_json.keys() or "description" not in request_json:
            raise APIException(Code.MISS_PARAM)
        if not request_json.get("env_name") or not request_json.get("description"):
            raise APIException(Code.VALUE_CANNOT_BE_NULL)
        EnvModel.add_env(request_json)
        return Result.success(data=request.json)

    def put(self):
        request_json = request.get_json()
        if not request_json:
            raise APIException(Code.MUST_JSON_DATA)
        id = request_json.get("id", None)
        if not id:
            raise APIException(Code.MISS_PARAM)
        env_name = request_json.get("env_name", None)
        description = request_json.get("description", None)
        if not env_name and not description:
            raise APIException(Code.MISS_PARAM)
        EnvModel.modify_env(id, env_name, description)
        return Result.success()

    def delete(self):
        request_json = request.json
        if not request_json:
            raise APIException(Code.MUST_JSON_DATA)
        id = request_json.get("id", None)
        if not id:
            raise APIException(Code.MISS_PARAM)
        EnvModel.delete_env(id)
        return Result.success()
