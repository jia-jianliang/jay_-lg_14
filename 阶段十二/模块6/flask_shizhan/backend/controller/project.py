from flask import request
from flask_restful import Resource

from backend.model.test_project import TestProject
from backend.util.response.APIException import NotFound, Code, APIException
from backend.util.response.result import Result


class Project(Resource):

    def get(self):
        query_type_dict = {
            "id": TestProject.get_by_id,
            "name": TestProject.get_by_name,
            "list": TestProject.get_list
        }
        query_data = request.args.to_dict()
        if not query_data:
            raise APIException(Code.MISS_PARAM)
        if not query_data.get("query_type", None):
            raise APIException(Code.MISS_PARAM)
        query_type = query_data.pop("query_type")
        if not query_type:
            raise APIException(Code.MISS_PARAM)
        if query_type not in query_type_dict:
            raise APIException(Code.MISS_PARAM)
        if query_type != "list":
            if not query_data.get(query_type):
                raise APIException(Code.VALUE_CANNOT_BE_NULL)
        model_func = query_type_dict.get(query_type)
        if query_type == "list":
            result = model_func(int(query_data.pop("page")), int(query_data.pop("size")))
        else:
            result = model_func(query_data.pop(query_type))
        return Result.success(data=result)

    def post(self):
        request_json = request.get_json()
        if not request_json:
            raise APIException(Code.MISS_PARAM)
        if "project_name" not in request_json.keys() or "description" not in request_json:
            raise APIException(Code.MISS_PARAM)
        if not request_json.get("project_name") or not request_json.get("description"):
            raise APIException(Code.VALUE_CANNOT_BE_NULL)
        TestProject.add_project(request_json)
        return Result.success(data=request.json)

    def put(self):
        request_json = request.get_json()
        if not request_json:
            raise APIException(Code.MUST_JSON_DATA)
        id = request_json.get("id", None)
        if not id:
            raise APIException(Code.MISS_PARAM)
        project_name = request_json.get("project_name", None)
        description = request_json.get("description", None)
        if not project_name and description:
            raise APIException(Code.MISS_PARAM)
        TestProject.modify_project(id, project_name, description)
        return Result.success()

    def delete(self):
        request_json = request.json
        if not request_json:
            raise APIException(Code.MUST_JSON_DATA)
        id = request_json.get("id", None)
        if not id:
            raise APIException(Code.MISS_PARAM)
        TestProject.delete_project(id)
        return Result.success()
