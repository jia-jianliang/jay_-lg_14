from flask import request
from flask_restful import Resource

from backend.util.response.APIException import APIException
from backend.util.response.code import Code
from backend.model.env_params import EnvParams as EnvParamsModel
from backend.util.response.result import Result


class EnvParams(Resource):
    def get(self):
        query_type_dict = {
            "id": EnvParamsModel.get_by_id,
            "name": EnvParamsModel.get_by_key,
            "list": EnvParamsModel.get_list
        }
        query_data = request.args.to_dict()
        if not query_data:
            raise APIException(Code.MISS_PARAM)
        if not query_data.get("query_type", None):
            raise APIException(Code.MISS_PARAM)
        query_type = query_data.pop("query_type")
        if not query_type:
            raise APIException(Code.MISS_PARAM)
        if query_type not in query_type_dict:
            raise APIException(Code.MISS_PARAM)
        if query_type != "list":
            if not query_data.get(query_type):
                raise APIException(Code.VALUE_CANNOT_BE_NULL)
        model_func = query_type_dict.get(query_type)
        if query_type == "list":
            result = model_func(int(query_data.pop("page")), int(query_data.pop("size")))
        else:
            result = model_func(query_data.pop(query_type))
        return Result.success(result)

    def post(self):
        request_json = request.get_json()
        if not request_json:
            raise APIException(Code.MISS_PARAM)
        if "params_key" not in request_json.keys() or "description" not in request_json:
            raise APIException(Code.MISS_PARAM)
        if not request_json.get("params_key") or not request_json.get("params_value") or not request_json.get("description"):
            raise APIException(Code.VALUE_CANNOT_BE_NULL)
        EnvParamsModel.add_env_params(request_json)
        return Result.success(data=request.json)

    def put(self):
        request_json = request.get_json()
        if not request_json:
            raise APIException(Code.MUST_JSON_DATA)
        id = request_json.get("id", None)
        if not id:
            raise APIException(Code.MISS_PARAM)
        params_key = request_json.get("params_key", None)
        params_value = request_json.get("params_value", None)
        description = request_json.get("description", None)
        env_id = request_json.get("env_id")
        if not params_key and not description:
            raise APIException(Code.MISS_PARAM)
        EnvParamsModel.modify_env_params(env_id, id, params_key, params_value, description)
        return Result.success()

    def delete(self):
        request_json = request.json
        if not request_json:
            raise APIException(Code.MUST_JSON_DATA)
        id = request_json.get("id", None)
        if not id:
            raise APIException(Code.MISS_PARAM)
        EnvParamsModel.delete_env_params(id)
        return Result.success()
