from flask import request
from flask_restful import Resource

from backend.model.test_suite import TestSuite
from backend.util.response.APIException import APIException
from backend.util.response.code import Code
from backend.util.response.result import Result


class Suite(Resource):

    def get(self):
        query_type_dict = {
            "id": TestSuite.get_by_id,
            "name": TestSuite.get_by_name,
            "list": TestSuite.get_list
        }
        query_data = request.args.to_dict()
        if not query_data:
            raise APIException(Code.MISS_PARAM)
        if not query_data.get("query_type", None):
            raise APIException(Code.MISS_PARAM)
        query_type = query_data.pop("query_type")
        if not query_type:
            raise APIException(Code.MISS_PARAM)
        if query_type not in query_type_dict:
            raise APIException(Code.MISS_PARAM)
        if query_type != "list":
            if not query_data.get(query_type):
                raise APIException(Code.VALUE_CANNOT_BE_NULL)
        model_func = query_type_dict.get(query_type)
        if query_type == "list":
            if "project_id" not in query_data:
                raise APIException(Code.MISS_PARAM)
            project_id = query_data.get("project_id")
            result = model_func(int(project_id), int(query_data.pop("page")), int(query_data.pop("size")))
        else:
            result = model_func(query_data.pop(query_type))
        return Result.success(data=result)

    def post(self):
        request_json = request.json
        if not request_json:
            raise APIException(Code.MUST_JSON_DATA)
        suite_name = request_json.get("suite_name")
        project_id = request_json.get("project_id")
        if not suite_name or not project_id:
            raise APIException(Code.MISS_PARAM)
        result = TestSuite.add_suite(request_json)
        return Result.success() if result else Result.fail(Code.FAIL_TO_ADD)

    def put(self):
        request_json = request.json
        if not request_json:
            raise APIException(Code.MISS_PARAM)
        suite_id = request_json.get("suite_id", None)
        project_id = request_json.get("project_id", None)
        suite_name = request_json.get("suite_name", None)
        description = request_json.get("description", None)
        if not suite_id or not project_id or not suite_name or not description:
            raise APIException(Code.VALUE_CANNOT_BE_NULL)
        result = TestSuite.modify_suite(suite_id, project_id, suite_name, description)
        return Result.success(data=request_json) if result else Result.fail()

    def delete(self):
        request_json = request.json
        if not request_json:
            raise APIException(Code.MISS_PARAM)
        if not request_json.get("suite_id", None):
            raise APIException(Code.VALUE_CANNOT_BE_NULL)
        suite_id = request_json.get("suite_id")
        result = TestSuite.delete_suite(suite_id)
        return Result.success() if result else Result.fail()
