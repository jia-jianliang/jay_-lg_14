from flask import request
from flask_restful import Resource

from backend.model.test_case import TestCase
from backend.util.response.APIException import APIException
from backend.util.response.code import Code
from backend.util.response.result import Result


class Case(Resource):
    def get(self):
        query_type_dict = {
            "id": TestCase.get_by_id,
            "name": TestCase.get_by_name,
            "list": TestCase.get_list
        }
        query_data = request.args.to_dict()
        if not query_data:
            raise APIException(Code.MISS_PARAM)
        if not query_data.get("query_type", None):
            raise APIException(Code.MISS_PARAM)
        query_type = query_data.pop("query_type")
        if not query_type:
            raise APIException(Code.MISS_PARAM)
        if query_type not in query_type_dict:
            raise APIException(Code.MISS_PARAM)
        if query_type != "list":
            if not query_data.get(query_type):
                raise APIException(Code.VALUE_CANNOT_BE_NULL)

        model_func = query_type_dict.get(query_type)
        if query_type == "list":
            suite_id = query_data.get("suite_id", None)
            if not suite_id:
                raise APIException(Code.VALUE_CANNOT_BE_NULL)
            result = model_func(suite_id, int(query_data.pop("page")), int(query_data.pop("size")))
        else:
            result = model_func(query_data.pop(query_type))
        return Result.success(data=result)

    def post(self):
        request_json = request.json
        if not request_json:
            raise APIException()
        case_name = request_json.get("case_name")
        suite_id = request_json.get("suite_id")
        method = request_json.get("method")
        if not case_name or not suite_id or not method:
            raise APIException(Code.MISS_PARAM)
        result = TestCase.add_case(request_json)
        return Result.success(data=request_json) if result else Result.fail()

    def put(self):
        request_json = request.json
        if not request_json:
            raise APIException(Code.MISS_PARAM)
        if not request_json.get("id", None):
            raise APIException(Code.VALUE_CANNOT_BE_NULL)
        result = TestCase.modify_case(request_json)
        return Result.success() if result else Result.fail()

    def delete(self):
        request_json = request.json
        id = request_json.get("id", None)
        if not request_json or not id:
            raise APIException(Code.MISS_PARAM)
        result = TestCase.delete_case(id)
        return Result.success() if result else Result.fail()
