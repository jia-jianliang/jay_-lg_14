from flask import request, make_response
from flask_login import UserMixin, login_user, logout_user
from flask_restful import Resource

from backend.app import login_manager
from backend.model.user import User as UserModel

from backend.util.response.APIException import APIException
from backend.util.response.code import Code
from backend.util.response.result import Result

@login_manager.user_loader
def user_load(username):
    user = UserModel.query_user(username)
    if user:
        current_user = UserModel()
        current_user.username = username
        return current_user

class User(Resource):
    def get(self):
        username = request.args.get("username")
        if username:
            print(request.url)
            user = UserModel.query_user(username)
            return Result.success(data=user)
        # return Result.fail()


    def post(self):
        request_json = request.get_json()
        if not request_json:
            raise APIException(Code.MISS_PARAM)
        if "username" not in request_json.keys():
            raise APIException(Code.MISS_PARAM)
        if not request_json.get("username"):
            raise APIException(Code.VALUE_CANNOT_BE_NULL)
        UserModel.add_user(request_json)
        return Result.success(data=request.json)

    def put(self):
        request_json = request.get_json()
        if not request_json:
            raise APIException(Code.MUST_JSON_DATA)
        id = request_json.get("id", None)
        if not id:
            raise APIException(Code.MISS_PARAM)
        user_id = request_json.get("id", None)
        if not id:
            raise APIException(Code.MISS_PARAM)
        UserModel.modify_user(request_json)
        return Result.success()

    def delete(self):
        request_json = request.json
        if not request_json:
            raise APIException(Code.MUST_JSON_DATA)
        id = request_json.get("id", None)
        if not id:
            raise APIException(Code.MISS_PARAM)
        UserModel.delete_user(id)
        return Result.success()

class Login(Resource):
    def post(self):
        request_json = request.get_json()
        if not request_json:
            raise APIException(Code.MISS_PARAM)
        if "username" not in request_json.keys() or "password" not in request_json.keys():
            raise APIException(Code.MISS_PARAM)
        if not request_json.get("username") or not request_json.get("password"):
            raise APIException(Code.VALUE_CANNOT_BE_NULL)
        username = request_json.get("username")
        password = request_json.get("password")
        user = UserModel.query_user(username)
        if user and user.get("password") == password:
            current_user = UserModel()
            current_user.id = username
            login_user(current_user, remember=True)
        # resp = make_response({"code": 0, "msg": "ok", "data": data})  # 设置响应体
        # resp.set_cookie("session", "123456", max_age=3600)
        # return Result.success(data=user)
        response = make_response({"code": 0, "msg": "ok", "data": user})
        response.set_cookie(key="key", value="value")
        return response

class Logout(Resource):
    def post(self):
        result = logout_user()
        if not result:
            return Result.fail()
        return Result.success()