from flask import request
from flask_restful import Resource

from backend.model.test_case import TestCase
from backend.util.response.APIException import APIException
from backend.util.response.code import Code
from backend.util.response.result import Result


class RunCase(Resource):

    def post(self):
        request_json = request.json
        if not request_json:
            raise APIException(Code.MISS_PARAM)
        if not request_json.get("suite_list", None) and not request_json.get("case_list", None):
            raise APIException(Code.VALUE_CANNOT_BE_NULL)
        case_list = request_json.get("case_list", None)
        suite_list = request_json.get("suite_list", None)
        if case_list:
            result = TestCase.run_case(request_json)
        if suite_list:
            result = TestCase.run_suite(request_json)
        return Result.success(data=result)