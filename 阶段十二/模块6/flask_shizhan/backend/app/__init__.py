import logging
from flask import Flask
from flask_login import LoginManager
from flask_restful import Api
from werkzeug.exceptions import HTTPException

from backend.config import USERNAME, PASSWORD, HOST, PORT, DATABASE
from backend.util.logging_util import init_logging
from flask_sqlalchemy import SQLAlchemy
# 初始化日志器
from backend.util.response.APIException import APIException
from backend.util.response.code import Code
from backend.util.response.result import Result


init_logging()
# 初始化flask对象
app = Flask(__name__)
# 配置sqlalchemy的uri（连接数据库的URL）
app.config['SQLALCHEMY_DATABASE_URI'] = f'mysql+pymysql://{USERNAME}:{PASSWORD}@{HOST}:{PORT}/{DATABASE}?charset=utf8'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SECRET_KEY'] = 'LKSDJFLKSDJFLKOWEIUBN'
# 创建SQLAlchemy的对象
db = SQLAlchemy(app)
handle_exception_tem = app.handle_exception
handle_user_excption_tem = app.handle_user_exception
api = Api(app)
app.handle_exception = handle_exception_tem
app.handle_user_exception = handle_user_excption_tem
login_manager = LoginManager()
login_manager.init_app(app)

from backend.util.cron_util import CronUtil

CronUtil.start_cron()

# @app.errorhandler(404)
# def abort_404(error):
#     return Result.fail(Code.NOT_FOUND)

# @app.errorhandler(Exception)
# def abort_server_error(error):
#     if isinstance(error, HTTPException):
#         code = error.code
#         msg = error.description
#         return APIException(code, msg)
#     return APIException(Code.MISS_PARAM)