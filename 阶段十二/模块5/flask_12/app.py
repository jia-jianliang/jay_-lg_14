from flask import Flask
from flask_login import LoginManager
from flask_migrate import Migrate

import config
from db import db
from entity.user import User
from user import user_module
from util.return_json import JsonFlask

app = JsonFlask(__name__)
app.register_blueprint(user_module, url_prefix="/user")
app.config.from_object(config)
db.init_app(app)
login_manager = LoginManager()
login_manager.init_app(app)
migrate = Migrate(app, db)

with app.app_context():
    db.create_all()
    # db.drop_all()


@login_manager.user_loader
def load_user(user_id):
    return User.query_user_by_id(int(user_id))

if __name__ == '__main__':

    app.run(debug=True)