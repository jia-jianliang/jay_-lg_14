import hashlib

from flask import request, current_app

from db import db
from entity.user import User
from entity.user import User
from user import user_module
from util.md5 import get_md5
from util.return_json import JsonResponse
from flask_login import login_user, current_user, login_required, logout_user
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer


@user_module.route("/add", methods=["POST"])
def regist():
    user_data = request.get_json()
    new_user = User(**user_data)
    db.session.add(new_user)
    db.session.commit()
    db.session.close()
    return JsonResponse.success(data=user_data)


@user_module.route("/login", methods=["POST"])
def login():
    if not verify_sign(request.get_json()):
        return JsonResponse.error(message="验签不通过")
    request_json = request.get_json()
    username = request_json.get("username")
    password = request_json.get("password")
    user_data = User.query_by_username(username)
    if user_data and password == user_data.password:
        user0 = User()
        user0.id = user_data.id
        login_user(user0)
        respons_data = {
            "token": gen_token(username).decode("UTF-8"),
            "id": user_data.id,
            "username": username,
            "password": password
        }
        return JsonResponse.success(message="登录成功", data=respons_data)
    return JsonResponse.error(message="用户名或者密码错误")

# 生成token
def gen_token(user):
    return Serializer(current_app.config["SECRET_KEY"], expires_in=7200).dumps({"id": user})


# 解析token
def parse_token(token):
    # 获取算法对象
    s = Serializer(current_app.config["SECRET_KEY"], expires_in=7200)
    # 解析token
    try:
        data = s.loads(token)
    except:
        return False
    # 返回解析之后的数据
    return True


# @login_manager.request_loader
@user_module.route("/detail/<uid>")
@login_required
def get_user_detail(uid):
    token = request.headers.get("token")
    if parse_token(token):
        user = User.query_user_by_id(uid)
        print(user)
        return JsonResponse.success(data=user.to_dict())
    return JsonResponse.error(message="token校验失败")


@user_module.route("/list")
@login_required
def list_user():
    token = request.headers.get("token")
    if parse_token(token):
        users = User.query_all_user()
        user_list = []
        user_dict = {}
        for user in users:
            user_list.append(
                {
                    "id": user.id,
                    "username": user.username,
                    "password": user.password
                }
            )
        return JsonResponse.success(data=user_list)
    return JsonResponse.error(message="token校验失败")


@user_module.route("/update/<uid>")
def update_user(uid):
    return {"msg": "成功了"}


@user_module.route("/logout", methods=["POST"])
@login_required
def logout():
    try:
        logout_user()
        return JsonResponse.success(message="登出成功")
    except:
        return JsonResponse.error(message="登出失败")


# @login_manager.unauthorized_handler
# def unauthor():
#     return JsonResponse.error(message="用户未登录")


# @login_manager.request_loader
# def load_user_from_request(request):
#     token = request.args.get("token")  # 既可以通过url传递token
#     if token:
#         data = parse_token(token)  # 解析token
#         if not data:  # 解析失败，直接返回None
#             return None
#         user = User.query_user(data.get("username"))  # 根据解析的token当中的数据，获取对应用户数据
#         if user:  # 如果查出数据，那么返回用户
#             user = User()
#             user.id = data.get("username")
#             return user
#     token = request.headers.get('token')  # 也可以通过请求头传递token
#     if token:
#         data = parse_token(token)
#         if not data:  # 解析失败，直接返回None
#             return None
#         user = User.query_by_username(data.get("username"))  # 根据解析的token当中的数据，获取对应用户数据
#         if user:  # 如果查出数据，那么返回用户
#             user = User()
#             user.id = data.get("username")
#             return user
#     return None





# 生成签名
def sign(origin_data):
# 排序
    sorted_data = sorted(origin_data)
    # 拼接字符串
    tempString = ''
    for data in sorted_data:
        tempString += data[0] + '=' + data[1] + '&'
    # 拼接SECRET_KEY
    print("拼接的字符串为：", tempString)
    # tempString = tempString + "api_key" + '=' + app.config["SECRET_KEY"]
    # print("得到的带有SECRET_KEY的字符串为：", tempString)
    # 进行签名运算（md5)
    def md5_data(tempString):
        m = hashlib.md5()
        m.update(tempString.encode("UTF-8"))
        return m.hexdigest()
    sign_data = md5_data(tempString.lower())
    return sign_data


def verify_sign(request_data):
    try:
        request_sign = request_data.pop("sign")
        print(request_sign)
        inner_sign = sign(request_data.items())
        return True if request_sign == inner_sign else False
    except:
        return False




