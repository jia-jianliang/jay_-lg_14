import hashlib


def get_md5(origin_data):
    m = hashlib.md5()
    m.update(origin_data.encode("UTF-8"))
    return m.hexdigest()
