from flask import Flask


class JsonFlask(Flask):

    def make_response(self, rv):
        if rv is None or isinstance(rv, (list, dict)):
            rv = JsonResponse.success(rv)
        if isinstance(rv, JsonResponse):
            rv = JsonResponse.to_dict(rv)
        return super().make_response(rv)


class JsonResponse:
    def __init__(self, code, message, data):
        self.code = code
        self.message = message
        self.data = data

    @classmethod
    def success(cls, code=1, message="成功", data=None):
        return JsonResponse(code, message, data)

    @classmethod
    def error(cls, code=0, message="失败", data=None):
        return JsonResponse(code, message, data)

    def to_dict(self):
        return {
            "code": self.code,
            "message": self.message,
            "data": self.data
        }

