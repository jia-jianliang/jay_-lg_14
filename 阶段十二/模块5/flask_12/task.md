1. 使用flask实现注册、登录、查询用户详情、查询用户列表、修改用户、登出接口 6个接口

要求：



a. 要对全部接口签名

b. 使用token技术，token放在headers中

c. 登录必须至少要有用户id、用户名、密码、描述信息四个参数

d. 返回数据统一格式为：

成功：{"status": 1,"msg":"成功", "data": {"xx":"xx"}}

失败：{"status": 0,"msg":"失败", "data": None}

e. 能用postman客户端调试通过

f. 使用json格式传递请求数据和响应数据

g. 用mysql数据库保存数据