from flask_login import UserMixin

from db import db


class User(db.Model, UserMixin):

    __tablename__ = 'user'

    id = db.Column('id', db.INTEGER, primary_key=True,
                   autoincrement=True, comment="用户id")
    username = db.Column('username', db.String(100),
                         nullable=False, comment="用户名")
    password = db.Column('password', db.String(100),
                         nullable=False, comment="密码")
    description = db.Column('description', db.String(500),
                            nullable=True, comment="描述")

    def __repr__(self):
        return f"User<{self.username}>"

    @staticmethod
    def query_by_username(username):
        return User.query.filter_by(username=username).first()

    @staticmethod
    def query_user_by_id(user_id):
        return User.query.filter_by(id=user_id).first()

    @classmethod
    def query_all_user(cls):
        return User.query.all()

    def to_dict(self):
        return {
            "id": self.id,
            "username": self.username,
            "password": self.password,
            "description": self.description
        }