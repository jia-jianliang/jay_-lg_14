(function (window) {
	'use strict';

	let fiters = {
		all(todos){
			return todos
		},
		active(todos){
			return todos.filter( todo => !todo.completed )
		},
		completed(todos){
			return todos.filter( todo => todo.completed)
		}
	}

	const TODOS_KEY = "todos_vue"
	let todoStorage = {
		get () {
			return JSON.parse(localStorage.getItem(TODOS_KEY)) || []
		},
		set(todos) {
			localStorage.setItem(TODOS_KEY, JSON.stringify(todos))
		}
	}
	// Your starting point. Enjoy the ride!
	new Vue({
		el: "#app",
		data: {
			// todos: [
			// 	{id: 1, title: "实例内容1", completed: true},
			// 	{id: 2, title: "实例内容2", completed: false},
			// 	{id: 3, title: "实例内容3", completed: false},
			// ],
			todos: todoStorage.get(),
			newTodo: "",
			editingTodo: null,
			titleBeforEdit: "",
			todoType: "all"
		},
		methods: {
			edit (todo) {
				if (!this.editingTodo) return
				this.editingTodo = null
				todo.title = todo.title.trim()
				if (!todo.title){
					this.remove(todo)
				}
			},
			cancelEdit (todo) {
				this.editingTodo = null
				todo.title = this.titleBeforEdit
			},
			editTodo (todo) {
				this.editingTodo = todo
				this.titleBeforEdit = todo.title
			},
			removeCompleted(){
				this.todos = this.todos.filter( todo => !todo.completed )
			},
			remove(todo) {
				var index = this.todos.indexOf(todo)
				this.todos.splice(index, 1)
			},
			pluralize (word) {
				return word + (this.remaining ===1 ? '': 's')
			},
			addTodo () {
				var value = this.newTodo.trim()
				if (!value) {
					return
				}
				this.todos.push({
					id: this.todos.length + 1,
					title: value,
					completed: false
				})
				this.newTodo = ''
			}
		},
		computed: {
			filterTodo(){
				return fiters[this.todoType](this.todos)
			},
			remaining () {
				return this.todos.filter(todo => !todo.completed).length
			},
			allDone: {
				get () {
					return this.remaining === 0
				},
				set (newValue) {
					this.todos.forEach( todo => todo.completed = newValue )
				}
			}
		},
		directives: {
			'todo-focus' (el, binding) {
				if (binding.value){
					el.focus()
				}
			}
		},
		watch: {
			todos: {
				deep: true,
				handler: todoStorage.set
			}
		}
	})

})(window);
