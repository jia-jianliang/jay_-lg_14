import os


def get_directory_size(path):
    if not path:
        raise Exception("请不要传递空值！")
    if not isinstance(path, str):
        raise Exception("请传递字符串类型参数！")
    if os.path.isfile(path):
        raise Exception("请传递文件夹路径！")
    if os.sep not in path:
        raise Exception("请输入绝对路径！")
    file_size = 0
    for base_dir,  dirs, files in os.walk(path):
        file_size += sum([os.path.getsize(os.path.join(base_dir, file))for file in files])
    return file_size


if __name__ == '__main__':

    # size = get_directory_size("D:\\develop\\python_project\\test")
    size = get_directory_size("C:\\Users\\gw00206773\\Desktop\\123")
    print(size)
