#### 简答题


###### 1、ADB的常见命令有哪些，至少8个
	adb devices：查看已连接设备列表
	adb connect ip port：链接某设备
	adb install 本地路径.apk：在设备上安装apk
	adb uninstall com.xxx.xxx(包名）：卸载apk
	adb logcat | findstr xxx > xxx.txt：将日志存到本地文件
	adb shell dumpsys | findstr Focused ：获取报名
	adb shell monkey -p com.xxx.xxx -v -v -v --ignore-timeouts --ignore-crashes -s 66 10000：运行monkey命令
	adb pull android路径 本地路径：将android文件发送到pc
	adb push 本地路径 android：将本地文件发送到android设备


###### 2. 在查看 logcat 命令日志时候怎么内容保存到本地文件？ 
	使用>将日志存储为文件，命令如下：
	adb logcat | findstr xxx > test.txt


###### 3、fiddler手机抓包的操作步骤是？
	1. 确保手机和电脑链接同一个网络
	2. fiddler设置允许https
	3. fiddler设置允许远程链接
	4. 设置手机无线，高级设置里选择手动代理，IP地址填写pc的ip，port填写fiddler的port
	5. 手机浏览器访问ip:port
	6. 在打开的页面点击链接下载https证书
	7. 手机操作app调用接口产生数据包
	8. fiddler查看数据包


###### 4、如何重现 ANR 和 crash 过程？
	1. 执行monkey时添加-s参数（s是seed的缩写，后面跟上相同的数值）比如 adb shell monkey -p com.xxx.xxx -v -v -v -throttle 500 1000 -s 66


###### 5、adb shell monkey -p com.flutter_luckin_coffee -v --throttle 50 --ignore-crashes --ignore-timeouts 100 > e:\apk\luckin.txt   解释下命令行参数的意思
	1. -p：表示时间数
	2. -v: 表示日志详细程度
	3. --throttle：表示事件间隔时间，单位毫秒
	4. --ignore-crashes：表示忽略崩溃
	5. --ignore-timeouts：表示忽略超时
	6. >:表示日志输出到文件