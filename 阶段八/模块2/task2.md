简答题


1.在appium自动化测试中，等待的类型有几种？区别是什么？
	Appium的等待机制跟Selenium等待机制相同都是分为以下三种
	1. 强制等待：运用变成语言的线程等待机制比如python的 time模块的 sleep()方法
	2. 隐式等待：全局性的等待，implicitly_wait(second)
	3. 显示等待：针对指定元素的等待机制，web Driver Wait(),经常搭配until使用


2.swipe与drag_and_drop滑动方法与TouchAction类中元素move_to方法有什么区别？
	1. swipe()和move_to是使用坐标进行滑动的, drap_and drop()是针对元素进行滑动的
	2. swipe是有惯性的，也就是说时间相同的情况下 滑动的距离越长造成的惯性越大，
	3. move_to()可以重复调用链接多个点


3.简述一下APPIUM工作原理
	appium的启动是在电脑上占用4723端口开启服务的
	我们用编程语言写的代码去访问appium的服务，并获得driver对象
	appium会将driver调用的方法转化成post请求发送给appium服务
	appium会将接收到的post请求发送给移动设备去执行
	