import pytest

import util.yaml_util
from util.random_util import random_email, random_username
from po.app import App


class TestLogin:

    def setup(self):
        self.app = App()

    def teardown(self):
        pass

    @pytest.mark.parametrize("username, password, email", util.yaml_util.get_data())
    def test_regist(self, username, password, email):
        username = random_username() + username
        email = random_email() + email
        self.app.start().main().goto_login().goto_regist().regist(username, password, email)