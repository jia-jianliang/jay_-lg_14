from appium import webdriver
from po.base import Base
from po.main import Main

class App(Base):

    def start(self):
        caps={}
        # 设备信息
        caps["platformName"] = "Android"
        caps["platformVersion"] = "5.1.1"
        caps["deviceName"] = "127.0.0.1:62001"
        # app信息
        caps["appPackage"] = "com.tal.kaoyan"
        caps["appActivity"] = "ui.activity.SplashActivity"
        # 中文
        caps["unicodeKeyboard"] = True
        caps["resetKeyboard"] = True
        #重置
        caps["noReset"]=False

        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', caps)
        self.driver.implicitly_wait(5)
        return self

    def main(self):
        return Main(self.driver)
