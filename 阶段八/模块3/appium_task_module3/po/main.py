from selenium.webdriver.common.by import By

from po.base import Base
from po.login import Login


class Main(Base):

    def goto_login(self):
        cancel="android:id/button2"
        skip="com.tal.kaoyan:id/tv_skip"

        # 判断取消按钮是否存在
        if self.is_exist(By.ID, cancel):
            self.find(By.ID, cancel).click()
        # 判断跳过是否存在
        if self.is_exist(By.ID, skip):
            self.find(By.ID, skip).click()
        return Login(self.driver)
