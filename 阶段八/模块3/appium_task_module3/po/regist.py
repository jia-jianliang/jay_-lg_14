from selenium.webdriver.common.by import By
from po.base import Base
from util.logger import Logger

log = Logger.get_logger()

class Regist(Base):

    def regist(self, username, password, email):
        _username = "com.tal.kaoyan:id/activity_register_username_edittext"
        _password = "com.tal.kaoyan:id/activity_register_password_edittext"
        _email = "com.tal.kaoyan:id/activity_register_email_edittext"
        _regist_btn = "com.tal.kaoyan:id/activity_register_register_btn"

        # 合作院校
        _academy = "com.tal.kaoyan:id/perfectinfomation_edit_school_name"
        sh = "com.tal.kaoyan:id/more_forum_title"
        _tongji = "com.tal.kaoyan:id/university_search_item_name"
        _major = "com.tal.kaoyan:id/activity_perfectinfomation_major"
        _major_value = "com.tal.kaoyan:id/major_subject_title"
        _tongjixue = "com.tal.kaoyan:id/major_group_title"
        _jingjitongji = "com.tal.kaoyan:id/major_search_item_name"
        _kaoyan_btn = "com.tal.kaoyan:id/activity_perfectinfomation_goBtn"

        # 输入用户名
        self.find(By.ID, _username).send_keys(username)
        log.info("输入用户名{}".format(username))
        # 输入密码
        self.find(By.ID, _password).send_keys(password)
        # 输入邮箱
        self.find(By.ID, _email).send_keys(email)
        log.info("输入邮箱{}".format(email))
        # 点击注册
        self.find(By.ID, _regist_btn).click()
        # 选择院校
        self.find(By.ID, _academy).click()
        # 选择具体院校
        self.find(By.ID, sh, 1).click()
        # 选择指定大学
        self.find(By.ID, _tongji, 1).click()
        # 选择专业
        self.find(By.ID, _major).click()
        # 具体学科
        self.find(By.ID, _major_value, 1).click()
        # 统计学
        self.find(By.ID, _tongjixue, 2).click()
        # 经济统计学
        self.find(By.ID, _jingjitongji, 1).click()
        # 进入考研帮
        self.find(By.ID, _kaoyan_btn).click()


