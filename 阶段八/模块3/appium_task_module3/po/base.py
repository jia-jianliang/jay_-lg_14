from appium.webdriver.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait


class Base:

    def __init__(self, driver:WebDriver=None):
        self.driver = driver

    def find(self, by, loc, index=0):
        # 当传递索引时，使用fand_elements寻找元素
        if index > 0:
            elements = WebDriverWait(self.driver, timeout=5).until(lambda x: x.find_elements(by, loc))
            return elements[index]
        return WebDriverWait(self.driver, timeout=5).until(lambda x: x.find_element(by, loc))

    def is_exist(self, by, loc):
        return True if self.find(by, loc) else False

