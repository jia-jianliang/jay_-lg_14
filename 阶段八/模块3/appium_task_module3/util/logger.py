import logging.handlers
class Logger:
    logger = None
    @classmethod
    def get_logger(cls):
        # logger 如果为空
        if cls.logger is None:
            # 获取日志器
            cls.logger = logging.getLogger()
            # 设置日志级别
            cls.logger.setLevel(logging.INFO)
            # 获取处理器 控制台
            sh = logging.StreamHandler()
            # 获取处理文件
            th = logging.handlers.TimedRotatingFileHandler(filename='../log/kaoyanbang.log',
                                                           when='midnight',
                                                           interval=1,
                                                           backupCount=30,
                                                           encoding='utf-8')


            # 获取格式器
            fm = '%(asctime)s %(levelname)s [%(name)s] [%(filename)s (%(funcName)s:%(lineno)d] - %(message)s'
            fmt = logging.Formatter(fm)

            # 格式器设置  处理器中
            sh.setFormatter(fmt)
            th.setFormatter(fmt)

            # 处理器添加日志器
            cls.logger.addHandler(sh)
            cls.logger.addHandler(th)

        return cls.logger