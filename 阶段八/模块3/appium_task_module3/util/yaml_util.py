import yaml


def get_data():
    with open("../data/regist.yaml", encoding="utf-8") as f:
        result = []
        dict_data = yaml.safe_load(f).get("test_data")
        for data in dict_data.keys():
            temp = []
            one_dict = dict_data.get(data)
            temp.append(one_dict.get("username"))
            temp.append(one_dict.get("password"))
            temp.append(one_dict.get("email"))
            result.append(temp)
    return result

