package test05;

public class Package {

    private double monthMoney;

    public Package(){}

    public Package(double monthMoney){
        setMonthMoney(monthMoney);
    }

    public double getMonthMoney(){
        return monthMoney;
    }
    public void setMonthMoney(double monthMoney){
        this.monthMoney = monthMoney;
    }

    public void show(){
        System.out.println(("我每月资费是：" + monthMoney));
    }
}
