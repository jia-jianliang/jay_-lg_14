package test05;

import java.util.Random;
import java.util.Scanner;

public class Test05 {
    /**
     * 定义一个长度为[16][16]的整型二维数组并输入或指定所有位置的元素值，分别实现二维数组中所有行和所有列中所有元素的累加和并打印。
     * 再分别实现二维数组中左上角到右下角和右上角到左下角所有元素的累加和并打印。
     * @return
     */
    public static void getArraySum(){
        //声明二维数组
        int[][] arr = new int[16][16];
        //随机数 用来给二位数组赋值
        Random random = new Random();
        //一维数组，用来记录每列加和
        int[] columnSum = new int[16];
        //记录从左上角到右下角加和
        int leftTop = 0;
        //记录从右上角到左下角加和
        int rightTop = 0;
        //循环赋值
        for (int i = 0; i < arr.length; i++){
            //存储每行的值
            int rowSum = 0;
            for (int j = 0; j < arr[i].length; j++){
                arr[i][j] = random.nextInt(16);
                columnSum[j] += arr[i][j];
                System.out.print(arr[i][j] + " ");
                rowSum += arr[i][j];
            }
            System.out.print("------------第" + (i+1) + "行的和是：" + rowSum);
            System.out.println();
            leftTop += arr[i][i];
            rightTop += arr[i][arr[i].length-1-i];
        }
        System.out.println("1到16列的和依次为");
        for (int i = 0; i < columnSum.length; i++){
            System.out.print(" " + columnSum[i]);
        }
        System.out.println();
        System.out.println("左上角到右下角的和是：" + leftTop);
        System.out.println("右上角到左下角的和是：" + rightTop);
    }

    public static void main(String[] args) {
/*
        1 定义一个长度为[16][16]的整型二维数组并输入或指定所有位置的元素值，分别实现二维数组中所有行和所有列中所有元素的累加和并打印。
          再分别实现二维数组中左上角到右下角和右上角到左下角所有元素的累加和并打印。
 */

        getArraySum();

        //2. 编程实现控制台版并支持两人对战的五子棋游戏。
    }
}
