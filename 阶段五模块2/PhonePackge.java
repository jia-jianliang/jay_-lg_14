package test05;

public class PhonePackge extends Package implements Call{
    //通话时长、短信条数、每月资费
    private double duration;
    private int msgCount;

    public PhonePackge(){}

    public PhonePackge(double duration, int msgCount, double monthMoney){
        super(monthMoney);
        setDuration(duration);
        setMsgCount(msgCount);
    }

    public double getDuration(){
        return duration;
    }
    public void setDuration(double duration){
        this.duration = duration;
    }
    public int getMsgCount(){
        return msgCount;
    }
    public void setMsgCount(int msgCount){
        this.msgCount= msgCount;
    }

    public void show(){
        super.show();
        System.out.println("我的通话时长是：" + duration + "短信条数是：" + msgCount);
    }

    public void callPhone(double duration, Card card) {
        card.setDate(duration);
        System.out.println(card);
    }
}
