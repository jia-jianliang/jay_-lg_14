package test05;

import com.sun.javaws.IconUtil;
import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.util.Scanner;

public class GoBang {

    //小棋盘 下棋用的棋盘
    private static char[][] arr = new char[16][16];
    //大棋盘 判断输赢用的棋盘
    private static char[][] biggerArr = new char[20][20];
    //false表示黑色 true表示白色
    private static boolean flag = true;

    //初始化成员变量
    static{
        //初始化棋盘
        for (int i = 0; i < arr.length; i++){
            for (int j = 0; j < arr[i].length; j++){
                arr[i][j] = '+';
            }
        }
        //初始化大棋盘
        for (int i = 0; i < biggerArr.length; i++){
            for (int j = 0; j < biggerArr[i].length; j++){
                biggerArr[i][j] = '+';
            }
        }
    }


    /**
     * 绘制棋盘
     */
    public void drawBoard(){
        System.out.println("   0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f");
        for (int i = 0; i < arr.length; i++){
            for (int j = 0; j < arr[i].length; j++){
                if (j == 0 && i < 10){
                    System.out.print(i + "  ");
                }
                if (j == 0 && i > 0){
                    switch(i){
                        case 10: System.out.print("a  "); break;
                        case 11: System.out.print("b  "); break;
                        case 12: System.out.print("c  "); break;
                        case 13: System.out.print("d  "); break;
                        case 14: System.out.print("e  "); break;
                        case 15: System.out.print("f  "); break;
                        default: break;
                    }
                }
                System.out.print(arr[i][j] + "  ");
            }
            System.out.println();
        }
    }

    /**
     * 下棋
     */
    public boolean playChess(){
        System.out.println("请输入坐标号投下一颗棋子");
        Scanner sc = new Scanner(System.in);
        String coord = sc.next();
        //将用户的输入转换成数字坐标 比如 11 代表是二维数组的第一行的下标为1的元素位置
        int row = Integer.parseInt(String.valueOf(coord.charAt(0)), 16);
        int column = Integer.parseInt(String.valueOf(coord.charAt(1)), 16);
        //将坐标对应成黑白棋并对二维数组赋值
        arr[row][column] = flag?'●':'○';
        biggerArr[row+4][column+4] = flag?'●':'○';
        //记录黑白棋 false黑棋  true白棋
        flag = !flag;
        //每下一颗棋 都会判断 对应路径上是否有五颗连续的棋子
        if (judgeVictory(row+4, column+4)){
            String winner = flag?"黑色":"白色";
            System.out.println( winner + "棋子赢了");
            return true;
        }
        drawBoard();
        return false;
    }

    /**
     * 判断输赢,任何棋子路线上只要有五个连续颜色的就是赢了
     * @return
     */
    public boolean judgeVictory(int row, int column){
        //声明容量为9的数组就是已投下的那颗棋子为中心 装下每条路径下的五子棋分布情况
        char[] rowDate = new char[9];//每行
        char[] columnDate = new char[9];//每列
        char[] leftTop = new char[9];//左上角到右下角
        char[] rightTop = new char[9];//右上角到左下角
        System.arraycopy(biggerArr[row], column-4, rowDate, 0, 9);

        //一轮循环 三条路径进行赋值
        int tem = 4;
        int temi = 0;
        for (int i = row-4; i <= row + 4; i++){
            columnDate[temi] = biggerArr[i][column];
            leftTop[temi] = biggerArr[i][column-tem];
            rightTop[temi] = biggerArr[i][column+tem];
            tem--;
            temi++;
        }
        //转换成字符串 为了方便判断是否包含五颗连续的棋子
        String rowStr = new String(rowDate);
        String columnStr = new String(columnDate);
        String leftStr = new String(leftTop);
        String rightStr = new String(rightTop);

        if (rowStr.contains("●●●●●") || rowStr.contains("○○○○○")){
            return true;
        }
        if (columnStr.contains("●●●●●") || columnStr.contains("○○○○○")){
            return true;
        }
        if (leftStr.contains("●●●●●") || leftStr.contains("○○○○○")){
            return true;
        }
        if (rightStr.contains("●●●●●") || rightStr.contains("○○○○○")){
            return true;
        }
        return false;
    }

    public static void main(String[] args){
        GoBang gb = new GoBang();
        while(true){
            gb.playChess();
        }
        }
    }

