package test05;

public class Test {

    public static void main(String[] args){
        System.out.println("===================================================================================");
        System.out.println("普通调用");
        Card card = new Card("中国联通", "18688888888", "周杰伦", "123456", 100.0, 10.0, 10.0);
        System.out.println(card);
        System.out.println("===================================================================================");
        System.out.println("普通调用");
        DatePackage datePackage = new DatePackage();
        datePackage.setMonthMoney(1.0);
        datePackage.setDate(1.0);
        datePackage.show();
        System.out.println("===================================================================================");
        System.out.println("普通调用");
        PhonePackge phonePackge = new PhonePackge();
        phonePackge.setMonthMoney(30.0);
        phonePackge.setMsgCount(15);
        phonePackge.setDuration(15.5);
        phonePackge.show();
        System.out.println("===================================================================================");
        System.out.println("使用父类引用的多态调用");
        Package p = new DatePackage(8.8, 9.9);
        p.show();
        System.out.println("===================================================================================");
        System.out.println("使用父类引用的多态调用");
        Package pp = new PhonePackge(1.0 , 2, 3.0);
        pp.show();
        System.out.println("===================================================================================");
        System.out.println("使用接口引用的多态调用");
        SurfInternet si = new DatePackage();
        si.goSurfInternet(6.6, card);
        System.out.println("===================================================================================");
        System.out.println("使用接口引用的多态调用");
        Call call = new PhonePackge(4.0, 5, 6.66);
        call.callPhone(888, card);
    }
}

