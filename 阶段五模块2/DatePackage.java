package test05;

public class DatePackage extends Package implements SurfInternet{
    //上网流量、每月资费
    private double date;

    public DatePackage(){}

    public DatePackage(double date, double monthMoney){
        super(monthMoney);
        setDate(date);
    }

    public double getDate(){
        return date;
    }
    public void setDate(double date){
        this.date = date;
    }

    public void show(){
        super.show();
        System.out.println("我的上网流量是：" + date);
    }

    public void goSurfInternet(double date, Card card) {
        card.setDate(date);
        System.out.println(card);
    }
}
