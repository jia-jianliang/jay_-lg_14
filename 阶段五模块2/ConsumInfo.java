package test05;

public class ConsumInfo {
    //统计通话时长、统计上网流量、每月消费金额
    private double duration;
    private double date;
    private double monthMoney;

    public ConsumInfo(){}

    public ConsumInfo(double duration, double date, double monthMoney){
        setDuration(duration);
        setDate(date);
        setMonthMoney(monthMoney);
    }
    public double getDuration(){
        return duration;
    }
    public void setDuration(double duration){
        this.duration = duration;
    }
    public double getDate(){
        return date;
    }
    public void setDate(double date){
        this.date = date;
    }
    public double getMonthMoney(){
        return monthMoney;
    }
    public void setMonthMoney(double monthMoney){
        this.monthMoney = monthMoney;
    }
}
