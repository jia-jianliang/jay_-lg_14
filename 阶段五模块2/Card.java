package test05;

public class Card {
    private String type;
    private String no;
    private String username;
    private String password;
    private double balance;
    private double duration;
    private double date;

    public Card(){}

    public Card(String type, String no, String username, String password, double balance, double duration, double date){
        setType(type);
        setNo(no);
        setUsername(username);
        setPassword(password);
        setBalance(balance);
        setDuration(duration);
        setDate(date);
    }

    public String getType(){
        return type;
    }
    public void setType(String type){
        this.type = type;
    }
    public String getNo(){
        return no;
    }
    public void setNo(String no){
        this.no = no;
    }
    public String getUsername(){
        return username;
    }
    public void setUsername(String username){
        this.username = username;
    }
    public String getPassword(){
        return password;
    }
    public void setPassword(String password){
        this.password = password;
    }
    public double getBalance(){
        return balance;
    }
    public void setBalance(double balance){
        this.balance = balance;
    }
    public double getDuration(){
        return duration;
    }
    public void setDuration(double duration){
        this.duration = duration;
    }
    public double getDate(){
        return date;
    }
    public void setDate(double date){
        this.date = date;
    }

    @Override
    public String toString(){
        //卡号 + 用户名 + 当前余额
        StringBuilder sb = new StringBuilder();
        sb.append("我的卡号是：");
        sb.append(no);
        sb.append("我的用户名是：");
        sb.append(username);
        sb.append("我卡里的余额是： ");
        sb.append(balance);
//        System.out.println(sb);
        return sb.toString();
    }

}
